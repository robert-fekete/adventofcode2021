﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day16
{
    class LiteralPacket : IPacket
    {
        public LiteralPacket(int version, long value)
        {
            Version = version;
            Value = value;
            SubPackets = Enumerable.Empty<IPacket>();
        }

        public long Value { get; }

        public int Version { get; }

        public IEnumerable<IPacket> SubPackets { get; }
    }
}
