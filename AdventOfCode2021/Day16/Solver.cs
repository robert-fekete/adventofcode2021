﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day16
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "847";

        public string SecondExpected => "333794664059";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "D2FE28"
                }, "6")
                .AddTestCase(new[]
                {
                   "8A004A801A8002F478"
                }, "16")
                .AddTestCase(new[]
                {
                   "620080001611562C8802118E34"
                }, "12")
                .AddTestCase(new[]
                {
                   "C0015000016115A2E0802F182340"
                }, "23")
                .AddTestCase(new[]
                {
                   "A0016C880162017C3686B18A3D4780"
                }, "31")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "C200B40A82"
                }, "3")
                .AddTestCase(new[]
                {
                    "04005AC33890"
                }, "54")
                .AddTestCase(new[]
                {
                    "880086C3E88112"
                }, "7")
                .AddTestCase(new[]
                {
                    "CE00C43D881120"
                }, "9")
                .AddTestCase(new[]
                {
                    "D8005AC2A8F0"
                }, "1")
                .AddTestCase(new[]
                {
                    "F600BC2D8F"
                }, "0")
                .AddTestCase(new[]
                {
                    "9C005AC2F8F0"
                }, "0")
                .AddTestCase(new[]
                {
                    "9C0141080250320F1802104A08"
                }, "1")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var message = new Message(input.First());
            var sequence = message.SequenceMessage();
            var parser = new PacketParser(sequence);

            var rootPacket = parser.Parse();

            var sumOfVersions = Traverse(rootPacket);

            return sumOfVersions.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var message = new Message(input.First());
            var sequence = message.SequenceMessage();
            var parser = new PacketParser(sequence);

            var rootPacket = parser.Parse();

            return rootPacket.Value.ToString();
        }

        private int Traverse(IPacket root)
        {
            var sum = 0;
            var backlog = new Stack<IPacket>();
            backlog.Push(root);

            while (backlog.Any())
            {
                var packet = backlog.Pop();

                sum += packet.Version;

                foreach(var next in packet.SubPackets)
                {
                    backlog.Push(next);
                }
            }

            return sum;
        }
    }
}
