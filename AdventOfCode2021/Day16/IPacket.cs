﻿using System.Collections.Generic;

namespace AdventOfCode2021.Day16
{
    interface IPacket
    {
        long Value { get; }
        int Version { get; }
        IEnumerable<IPacket> SubPackets { get; }
    }
}
