﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day16
{
    internal class Message
    {
        private string rawMessage;

        public Message(string hexa)
        {
            rawMessage = hexa;
        }

        internal Sequence SequenceMessage()
        {
            var bits = new List<bool>();
            foreach (var c in rawMessage)
            {
                int hexaValue = Convert.ToInt32(c.ToString(), 16);
                bits.Add((hexaValue & 8) != 0);
                bits.Add((hexaValue & 4) != 0);
                bits.Add((hexaValue & 2) != 0);
                bits.Add((hexaValue & 1) != 0);
            }

            return new Sequence(bits);
        }
    }
}