﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day16
{
    internal class PacketParser
    {
        private readonly Sequence sequence;

        public PacketParser(Sequence sequence)
        {
            this.sequence = sequence;
        }
        public IPacket Parse()
        {
            var version = sequence.Get(3).AsInt();
            var type = sequence.Get(3).AsInt();

            if (type == 4)
            {
                var value = 0L;
                var last = false;
                while (!last)
                {
                    last = !sequence.Get(1).AsBool();
                    value *= 16;
                    value += sequence.Get(4).AsLong();
                }

                return new LiteralPacket(version, value);
            }
            else
            {
                var subPackets = ParseSubPackets();

                switch (type)
                {
                    case 0:
                        return new CompositePacket(version, sp => sp.Sum(p => p.Value), subPackets);
                    case 1:
                        return new CompositePacket(version, sp => sp.Aggregate(1L, (a, p) => a * p.Value), subPackets);
                    case 2:
                        return new CompositePacket(version, sp => sp.Min(p => p.Value), subPackets);
                    case 3:
                        return new CompositePacket(version, sp => sp.Max(p => p.Value), subPackets);
                    case 5:
                        return new CompositePacket(version, sp => sp.First().Value > sp.Skip(1).First().Value ? 1 : 0, subPackets);
                    case 6:
                        return new CompositePacket(version, sp => sp.First().Value < sp.Skip(1).First().Value ? 1 : 0, subPackets);
                    case 7:
                        return new CompositePacket(version, sp => sp.First().Value == sp.Skip(1).First().Value ? 1 : 0, subPackets);
                    default:
                        throw new InvalidOperationException("Invalid type ID");
                }
            }
        }

        private IEnumerable<IPacket> ParseSubPackets()
        {
            var isPacketCount = sequence.Get(1).AsBool();
            if (isPacketCount)
            {
                var numberOfSubPackets = sequence.Get(11).AsInt();

                var subPackets = new List<IPacket>();
                for (int i = 0; i < numberOfSubPackets; i++)
                {
                    var subPacketParser = new PacketParser(sequence);
                    subPackets.Add(subPacketParser.Parse());
                }

                return subPackets;
            }
            else
            {
                var totalLength = sequence.Get(15).AsInt();
                var subPacketsSequence = sequence.Get(totalLength);
                var subPackets = new List<IPacket>();
                while (!subPacketsSequence.IsFinished)
                {
                    var subPacketParser = new PacketParser(subPacketsSequence);
                    subPackets.Add(subPacketParser.Parse());
                }

                return subPackets;
            }
        }
    }
}