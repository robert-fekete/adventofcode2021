﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day16
{
    class CompositePacket : IPacket
    {
        private readonly Func<IEnumerable<IPacket>, long> aggregator;

        public CompositePacket(int version, Func<IEnumerable<IPacket>, long> aggregator, IEnumerable<IPacket> subPackets)
        {
            Version = version;
            this.aggregator = aggregator;
            SubPackets = subPackets;
        }

        public long Value => aggregator(SubPackets);
        public int Version { get; }
        public IEnumerable<IPacket> SubPackets { get; }
    }
}
