﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day16
{
    class Sequence
    {
        private readonly bool[] data;
        private int iterator;
        public Sequence(IEnumerable<bool> data)
        {
            this.data = data.ToArray();
        }

        public bool IsFinished => iterator >= data.Length || data.All(b => !b);

        public Sequence Get(int length)
        {
            var newSequence = data.Skip(iterator).Take(length);
            iterator += length;

            return new Sequence(newSequence);
        }

        public int AsInt()
        {
            if (data.Count() > 32)
            {
                throw new InvalidOperationException("Sequence is too long to convert it to Int32");
            }

            return (int)AsLong();
        }

        public long AsLong()
        {
            if (data.Count() > 64)
            {
                throw new InvalidOperationException("Sequence is too long to convert it to Int64");
            }

            var result = 0L;
            foreach (var bit in data)
            {
                result *= 2;
                result += bit ? 1 : 0;
            }

            return result;
        }

        public bool AsBool()
        {
            if (data.Count() > 1)
            {
                throw new InvalidOperationException("Sequence is too long to convert it to Boolean");
            }

            return data[0];
        }

        public override string ToString()
        {
            return string.Join("", data.Select(b => b ? "1" : "0"));
        }
    }
}
