﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day3
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "4160394";

        public string SecondExpected => "4125600";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "00100",
                    "11110",
                    "10110",
                    "10111",
                    "10101",
                    "01111",
                    "00111",
                    "11100",
                    "10000",
                    "11001",
                    "00010",
                    "01010",
                }, "198")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "00100",
                    "11110",
                    "10110",
                    "10111",
                    "10101",
                    "01111",
                    "00111",
                    "11100",
                    "10000",
                    "11001",
                    "00010",
                    "01010",
                }, "230")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var gammaRate = 0;
            var epsilonRate = 0;

            var length = input.First().Length;
            for (int i = 0; i < length; i++)
            {
                (var mostCommonBit, var leastCommonBit) = FindMostAndLeastCommonBits(input, i);

                gammaRate = gammaRate * 2 + mostCommonBit;
                epsilonRate = epsilonRate * 2 + leastCommonBit;
            }

            var powerConsumption = gammaRate * epsilonRate;
            return powerConsumption.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var length = input.First().Length;
            var oxygenRating = GetLifeSupportRatingFactor(input, length, p => p.MostCommonBit);
            var CO2Rating = GetLifeSupportRatingFactor(input, length, p => p.LeastCommonBit);

            var lifeSupportRating = oxygenRating * CO2Rating;

            return lifeSupportRating.ToString();
        }

        // Could maintain two lists (for oxygen and CO2) in this method, so we don't have to iterate through the diagnostic report twice
        // I went with readability over performance as it's O(n*m) either way
        private int GetLifeSupportRatingFactor(IEnumerable<string> input, int length, Func<(int MostCommonBit, int LeastCommonBit), int> chooseBit)
        {
            var current = new List<string>(input);

            for (int i = 0; i < length; i++)
            {
                var bits = FindMostAndLeastCommonBits(current, i);
                var significantValue = chooseBit(bits) + '0';

                var nextValues = current.Where(l => l[i] == significantValue);
                current = new List<string>(nextValues);

                if (current.Count == 1)
                {
                    break;
                }
            }

            return Convert.ToInt32(current.Single(), 2);
        }

        private static (int, int) FindMostAndLeastCommonBits(IEnumerable<string> input, int i)
        {
            var ones = 0;
            var zeros = 0;
            foreach (var line in input)
            {
                if (line[i] == '0')
                {
                    zeros++;
                }
                else if (line[i] == '1')
                {
                    ones++;
                }
                else
                {
                    throw new InvalidOperationException("Invalid character");
                }
            }

            return ones >= zeros ? (1, 0) : (0, 1);
        }
    }
}
