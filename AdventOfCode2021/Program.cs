﻿using AdventOfCode.Framework;

namespace AdventOfCode2021
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = RunnerFactory.Create(typeof(Program).Assembly);

            var report = new Report();
            report.Start();

            runner.RunAll();
            
            report.Stop();
            report.Print();
        }
    }
}
