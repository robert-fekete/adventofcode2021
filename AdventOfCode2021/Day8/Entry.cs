﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day8
{
    class Entry
    {
        private readonly string[] signal;
        private readonly string[] output;

        public Entry(string[] signal, string[] output)
        {
            this.signal = signal;
            this.output = output;
        }

        public int UniqueDigits => output.Count(s => s.Length == 2 || s.Length == 4 || s.Length == 3 || s.Length == 7);

        public int DecypherOutput()
        {
            var digitMapping = GetDigitsDictionary();
            var decypheredOutput = 0;
            foreach (var digit in output)
            {
                decypheredOutput *= 10;

                var key = string.Concat(digit.OrderBy(c => c));
                decypheredOutput += digitMapping[key];
            }

            //Console.WriteLine($"{string.Join(" ", output)}: {decypheredOutput}");
            return decypheredOutput;
        }

        private Dictionary<string, int> GetDigitsDictionary()
        {
            var one = signal.Single(s => s.Length == 2);
            var four = signal.Single(s => s.Length == 4);
            var seven = signal.Single(s => s.Length == 3);

            var mapping = new Dictionary<string, int>();
            foreach(var word in signal)
            {
                var value = -1;
                if (word.Length == 6 && one.All(c => word.Contains(c)) && !four.All(c => word.Contains(c)))
                {
                    value = 0;
                }
                else if (word == one)
                {
                    value = 1;
                }
                else if(word.Length == 5 && word.Concat(four).Distinct().Count() == 7)
                {
                    value = 2;
                }
                else if(word.Length == 5 && seven.All(c => word.Contains(c)))
                {
                    value = 3;
                }
                else if(word == four)
                {
                    value = 4;
                }
                else if(word.Length == 5 && word.Concat(four).Distinct().Count() == 6 && !seven.All(c => word.Contains(c)))
                {
                    value = 5;
                }
                else if(word.Length == 6 && !one.All(c => word.Contains(c)) && !four.All(c => word.Contains(c)))
                {
                    value = 6;
                }
                else if(word == seven)
                {
                    value = 7;
                }
                else if(word.Length == 7)
                {
                    value = 8;
                }
                else if(word.Length == 6 && four.All(c => word.Contains(c)))
                {
                    value = 9;
                }
                else
                {
                    throw new InvalidOperationException("What is the meaning of this?");
                }

                var key = string.Concat(word.OrderBy(c => c));
                mapping[key] = value;
            }

            return mapping;
        }
    }
}
