﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day5
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "5608";

        public string SecondExpected => "20299";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "0,9 -> 5,9",
                    "8,0 -> 0,8",
                    "9,4 -> 3,4",
                    "2,2 -> 2,1",
                    "7,0 -> 7,4",
                    "6,4 -> 2,0",
                    "0,9 -> 2,9",
                    "3,4 -> 1,4",
                    "0,0 -> 8,8",
                    "5,5 -> 8,2",
                }, "5")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "0,9 -> 5,9",
                    "8,0 -> 0,8",
                    "9,4 -> 3,4",
                    "2,2 -> 2,1",
                    "7,0 -> 7,4",
                    "6,4 -> 2,0",
                    "0,9 -> 2,9",
                    "3,4 -> 1,4",
                    "0,0 -> 8,8",
                    "5,5 -> 8,2",
                }, "12")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var lines = ParseLines(input);

            var count = CountDangerZones(lines, line => line.IsHorizontal || line.IsVertical);

            return count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var lines = ParseLines(input);

            var count = CountDangerZones(lines, _ => true);

            return count.ToString();
        }

        private int CountDangerZones(IEnumerable<Line> lines, Predicate<Line> filter)
        {
            var counter = new Counter();

            foreach (var line in lines)
            {
                foreach (var point in line.GetPoints())
                {
                    if (filter(line))
                    {
                        counter.Increment(point);
                    }
                }
            }

            var count = counter.CountLargerThan(2);
            return count;
        }

        private IEnumerable<Line> ParseLines(IEnumerable<string> input)
        {
            var lines = input.Select(l => l.Split(" -> "))
                             .Select(p => (p[0].Split(','), p[1].Split(',')))
                             .Select(p => new Line((int.Parse(p.Item1[0]), int.Parse(p.Item1[1])), (int.Parse(p.Item2[0]), int.Parse(p.Item2[1]))));

            return lines;
        }
    }
}
