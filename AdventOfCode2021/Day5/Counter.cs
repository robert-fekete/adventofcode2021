﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day5
{
    class Counter
    {
        private Dictionary<(int X, int Y), int> points = new Dictionary<(int, int), int>();
        internal void Increment((int X, int Y) point)
        {
            if (!points.ContainsKey(point))
            {
                points[point] = 0;
            }

            points[point]++;
        }

        internal int CountLargerThan(int value)
        {
            return points.Count(kvp => kvp.Value >= value);
        }

        internal void Print()
        {
            var minX = points.Keys.Select(p => p.X).Min();
            var maxX = points.Keys.Select(p => p.X).Max();
            var minY = points.Keys.Select(p => p.Y).Min();
            var maxY = points.Keys.Select(p => p.Y).Max();

            for(int y = minY; y <= maxY; y++)
            {
                for(int x = minX; x <= maxX; x++)
                {
                    Console.Write(points.ContainsKey((x, y)) ? points[(x, y)].ToString() : '.');
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
