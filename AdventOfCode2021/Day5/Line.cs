﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day5
{
    class Line
    {
        private readonly (int X, int Y) start;
        private readonly (int X, int Y) end;
        private readonly (int dx, int dy) step;

        public Line((int X, int Y) start, (int X, int Y) end)
        {
            this.start = start;
            this.end = end;

            step = CalculateStep(start, end);
        }

        public bool IsHorizontal => start.Y == end.Y;
        public bool IsVertical => start.X == end.X;

        public IEnumerable<(int X, int Y)> GetPoints()
        {
            var x = start.X;
            var y = start.Y;

            var points = new List<(int X, int Y)>();

            points.Add((x, y));
            while (x != end.X || y != end.Y)
            {
                x += step.dx;
                y += step.dy;

                points.Add((x, y));
            }

            return points;
        }

        private (int dx, int dy) CalculateStep((int X, int Y) start, (int X, int Y) end)
        {
            var horizontalDiff = Math.Abs(start.X - end.X);
            var verticalDiff = Math.Abs(start.Y - end.Y);

            var diff = Math.Max(horizontalDiff, verticalDiff);

            var stepX = (end.X - start.X) / diff;
            var stepY = (end.Y - start.Y) / diff;

            return (stepX, stepY);
        }
    }
}
