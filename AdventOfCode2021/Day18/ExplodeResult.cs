﻿namespace AdventOfCode2021.Day18
{
    class ExplodeResult
    {
        public ExplodeResult(bool isExploded, INode newNode, long? passLeft, long? passRight)
        {
            IsExploded = isExploded;
            NewNode = newNode;
            PassLeft = passLeft;
            PassRight = passRight;
        }

        public bool IsExploded { get; }
        public INode NewNode { get; }
        public long? PassLeft { get; }
        public long? PassRight { get; }

        public static ExplodeResult WithoutExplode(INode node) => new ExplodeResult(false, node, null, null);
        public ExplodeResult WithoutLeft(INode node) => new ExplodeResult(IsExploded, node, null, PassRight);
        public ExplodeResult WithoutRight(INode node) => new ExplodeResult(IsExploded, node, PassLeft, null);
    }
}
