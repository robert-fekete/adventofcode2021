﻿import math

class Node:
    def __init__(self):
        self.left = None
        self.right = None
        self.parent = None
       
    def isSimple(self):
        return False
   
    def getMagnitude(self):
       
        return self.left.getMagnitude() * 3 + self.right.getMagnitude() * 2
       
    def __str__(self):
        return self.str(0, True)
       
    def str(self, depth, isSimplePrint):
       
        if isSimplePrint:
            return "[" + self.left.str(depth, True) + "," + self.right.str(depth, True) + "]"
        else:
            return ("." * depth) + "[\n" + self.left.str(depth + 1) + "\n" + self.right.str(depth + 1) + "\n" + ("." * depth) + "]"
       
    def add(self, other):
        node = Node()
        node.left = self
        self.parent = node
        node.right = other
        other.parent = node
       
        node.reduce()
        return node
       
    def reduce(self):
       
        toContinue = True
        while toContinue:
            #print(self)
            toContinue = False
            if self.explode(1)[0]:
                toContinue = True
                continue
            if self.split()[0]:
                toContinue = True
   
    def explode(self, depth):
       
        if self.left:
            (isExploded, new_left) = self.left.explode(depth + 1)
            if isExploded:
                self.left = new_left
                return (True, self)
           
        if self.left.isSimple() and self.right.isSimple() and depth > 4:
            if self.parent:
                self.addRight(self.left.value)
                self.addLeft(self.right.value)
            return (True, SimpleNode(0))
           
        if self.right:
            (isExploded, new_right) = self.right.explode(depth + 1)
            if isExploded:
                self.right = new_right
                return (True, self)
           
        return (False, self)
       
    def addLeft(self, value):
       
        right = None
        current = self
        while current.parent is not None:
            if current.parent.right != current:
                right = current.parent.right
                break
            else:
                current = current.parent
       
        if right is None:
            return
       
        current = right
        while not current.isSimple():
            current = current.left
           
        current.value += value
       
    def addRight(self, value):
       
        left = None
        current = self
        while current.parent is not None:
            if current.parent.left != current:
                left = current.parent.left
                break
            else:
                current = current.parent
       
        if left is None:
            return
       
        current = left
       
        while not current.isSimple():
            current = current.right
           
        current.value += value
       
    def split(self):
       
        if self.left:
            (isSplit, new_left) = self.left.split()
            if isSplit:
                new_left.parent = self
                self.left = new_left
                return (True, self)
        if self.right:
            (isSplit, new_right) = self.right.split()
            if isSplit:
                new_right.parent = self
                self.right = new_right
                return (True, self)
           
        return (False, self)
       
class SimpleNode:
    def __init__(self, value):
        self.value = value
       
    def isSimple(self):
        return True
       
    def getMagnitude(self):
        return self.value
       
    def split(self):
        if self.value < 10:
            return (False, self)
       
        left = SimpleNode(math.floor(self.value / 2))
        right = SimpleNode(math.ceil(self.value / 2))
        node = Node()
        node.left = left
        node.right = right
       
        return (True, node)
       
    def explode(self, depth):
        return (False, self)
       
    def str(self, depth, isSimplePrint):
       
        if isSimplePrint:
            return str(self.value)
        else:
            return ("." * depth) + str(self.value)
       
def parse(lines):
   
    return [parseNumber(l) for l in lines]
   
def parseNumber(line):
   
    backlog = []
    current = None
    value = None
    parts = tokenize(line)
    for part in parts:
        if part == "[":
            backlog.append(current)
            new_current = Node()
            new_current.parent = current
            current = new_current
        elif part == ",":
            current.left = value
        elif part == "]":
            current.right = value
            value = current
            current = backlog.pop()
        else:
            value = SimpleNode(part)
           
    return value
           
       
def tokenize(line):
    tokens = []
    buffer = ""
    for c in line:
        if c == "[":
            if buffer != "":
                tokens.append(int(buffer))
                buffer = ""
            tokens.append(c)
        elif c == "]":
            if buffer != "":
                tokens.append(int(buffer))
                buffer = ""
            tokens.append(c)
        elif c == ",":
            if buffer != "":
                tokens.append(int(buffer))
                buffer = ""
            tokens.append(c)
        else:
            buffer += c
           
    return tokens
   
inp = open(r".\input.txt", 'r').read()
numbers = parse(inp.split("\n"))
iter = numbers[0]
for n in numbers[1:]:
    iter = iter.add(n)
   
print(iter.getMagnitude())

original_numbers = parse(inp.split("\n"))

lines = inp.split("\n")
maxMagnitude = 0
for i in range(len(lines)):
    for j in range(len(lines)):
        if i != j:
            n1 = parseNumber(lines[i])
            n2 = parseNumber(lines[j])
            result = n1.add(n2)
            magnitude = result.getMagnitude()
            if magnitude > maxMagnitude:
                maxMagnitude = magnitude
           
print(maxMagnitude)