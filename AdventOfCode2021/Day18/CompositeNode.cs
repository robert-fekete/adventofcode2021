﻿namespace AdventOfCode2021.Day18
{
    class CompositeNode : INode
    {
        private INode left;
        private INode right;

        public CompositeNode(INode left, INode right)
        {
            this.left = left;
            this.right = right;
        }

        public bool IsSimple => false;
        public long Magnitude => 3 * left.Magnitude + 2 * right.Magnitude;

        public void Reduce()
        {
            var isChanged = true;
            while (isChanged)
            {
                isChanged = false;
                var result = Explode(1);
                if (result.IsExploded)
                {
                    isChanged = true;
                    continue;
                }

                (var isSplitted, var _) = Split();
                isChanged = isSplitted;
            }
        }

        public ExplodeResult Explode(int level)
        {
            var result = left.Explode(level + 1);
            if (result.IsExploded)
            {
                left = result.NewNode;
                if (result.PassRight.HasValue)
                {
                    right.AddLeft(result.PassRight.Value);
                }
                return result.WithoutRight(this);
            }

            if (level > 4 && left.IsSimple && right.IsSimple)
            {
                return new ExplodeResult(true, new SimpleNode(0), left.Magnitude, right.Magnitude);
            }

            result = right.Explode(level + 1);
            if (result.IsExploded)
            {
                right = result.NewNode;
                if (result.PassLeft.HasValue)
                {
                    left.AddRight(result.PassLeft.Value);
                }
                return result.WithoutLeft(this);
            }

            return ExplodeResult.WithoutExplode(this);
        }

        public void AddLeft(long value)
        {
            left.AddLeft(value);
        }

        public void AddRight(long value)
        {
            right.AddRight(value);
        }

        public (bool, INode) Split()
        {
            (var isSplitted, var newNode) = left.Split();
            if (isSplitted)
            {
                left = newNode;
                return (true, this);
            }

            (isSplitted, newNode) = right.Split();
            if (isSplitted)
            {
                right = newNode;
                return (true, this);
            }

            return (false, this);
        }

        public INode Add(INode other)
        {
            var newNode = new CompositeNode(this.Clone(), other.Clone());
            newNode.Reduce();
            return newNode;
        }

        public INode Clone()
        {
            return new CompositeNode(left.Clone(), right.Clone());
        }

        public override string ToString()
        {
            return $"[{left},{right}]";
        }
    }
}
