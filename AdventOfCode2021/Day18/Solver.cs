﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day18
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "4417";

        public string SecondExpected => "4796";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestBuilder(builder =>
                    builder.AddTestCase("[[[[[9,8],1],2],3],4]", "[[[[[9,8],1],2],3],4]")
                           .AddTestCase("[1,2]", "[1,2]")
                           .AddTestCase("[[1,2],3]", "[[1,2],3]")
                           .AddTestCase("[9,[8,7]]", "[9,[8,7]]")
                           .AddTestCase("[[1,9],[8,5]]", "[[1,9],[8,5]]")
                           .AddTestCase("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]", "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]")
                           .AddTestCase("[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]", "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]")
                           .AddTestCase("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]", "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]")
                           .AddTest(s => new NodeFactory().CreateNode(s).ToString()))

                .AddTestBuilder(builder =>
                    builder.AddTestCase("[1,[[3,[2,0]],[9,[5,[4,[3,2]]]]]]", "[1,[[3,[2,0]],[9,[5,[7,0]]]]]")
                           .AddTestCase("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]")
                           .AddTestCase("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]")
                           .AddTestCase("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]")
                           .AddTestCase("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")
                           .AddTestCase("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")
                           .AddTest(s => new NodeFactory().CreateNode(s).Explode(1).NewNode.ToString()))

                .AddTestBuilder(builder =>
                    builder.AddTestCase("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")
                           .AddTestCase("[[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]", "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]")
                           .AddTest(s =>
                           {
                               var node = new NodeFactory().CreateNode(s);
                               node.Reduce();
                               return node.ToString();
                           }))

                .AddTestBuilder(builder =>
                    builder.AddTestCase(new[]
                            {
                                "[1, 1]",
                                "[2, 2]",
                                "[3, 3]",
                                "[4, 4]",
                            }, "[[[[1,1],[2,2]],[3,3]],[4,4]]")
                            .AddTestCase(new[]
                            {
                                "[1, 1]",
                                "[2, 2]",
                                "[3, 3]",
                                "[4, 4]",
                                "[5, 5]",
                            }, "[[[[3,0],[5,3]],[4,4]],[5,5]]")
                            .AddTestCase(new[]
                            {
                                "[1, 1]",
                                "[2, 2]",
                                "[3, 3]",
                                "[4, 4]",
                                "[5, 5]",
                                "[6, 6]",
                            }, "[[[[5,0],[7,4]],[5,5]],[6,6]]")
                            .AddTestCase(new[]
                            {
                                "[[[0,[4, 5]],[0, 0]],[[[4, 5],[2, 6]],[9,5]]]",
                                "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
                                "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]",
                                "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]",
                                "[7,[5,[[3,8],[1,4]]]]",
                                "[[2,[2,2]],[8,[8,1]]]",
                                "[2,9]",
                                "[1,[[[9,3],9],[[9,0],[0,7]]]]",
                                "[[[5,[7,4]],7],1]",
                                "[[[[4,2],2],6],[8,7]]",
                            }, "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")
                           .AddTest(numbers => 
                           {
                               var nodes = numbers.Select(s => new NodeFactory().CreateNode(s));
                               return nodes.Skip(1).Aggregate(nodes.First(), (acc, n) => acc.Add(n)).ToString();
                           }))

                .AddTestBuilder(builder =>
                    builder.AddTestCase(new[]
                            {
                                "[[[0,[5, 8]],[[1, 7],[9, 6]]],[[4,[1,2]],[[1,4],2]]]",
                                "[[[5,[2,8]],4],[5,[[9,9],0]]]",
                                "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
                                "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
                                "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
                                "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
                                "[[[[5,4],[7,7]],8],[[8,3],8]]",
                                "[[9,3],[[9,9],[6,[4,9]]]]",
                                "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
                                "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
                            }, "4140")
                    .AddTest(ExecuteFirst))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "[[[0,[5, 8]],[[1, 7],[9, 6]]],[[4,[1,2]],[[1,4],2]]]",
                    "[[[5,[2,8]],4],[5,[[9,9],0]]]",
                    "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
                    "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
                    "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
                    "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
                    "[[[[5,4],[7,7]],8],[[8,3],8]]",
                    "[[9,3],[[9,9],[6,[4,9]]]]",
                    "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
                    "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
                }, "3993")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var factory = new NodeFactory();
            var nodes = input.Select(s => factory.CreateNode(s));

            var result = nodes.Skip(1).Aggregate(nodes.First(), (acc, node) => acc.Add(node));

            return result.Magnitude.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var factory = new NodeFactory();
            var lines = input.ToArray();
            var nodes = input.Select(s => factory.CreateNode(s)).ToArray();

            var maxMagnitude = 0L;
            for(int i = 0; i < nodes.Length; i++)
            {
                for(int j = 0; j < nodes.Length; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    var result = nodes[i].Add(nodes[j]);
                    if (maxMagnitude < result.Magnitude)
                    {
                        maxMagnitude = result.Magnitude;
                    }
                }
            }

            return maxMagnitude.ToString();
        }
    }
}
