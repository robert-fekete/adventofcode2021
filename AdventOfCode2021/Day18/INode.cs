﻿namespace AdventOfCode2021.Day18
{
    interface INode
    {
        bool IsSimple { get; }
        long Magnitude { get; }
        (bool, INode) Split();
        ExplodeResult Explode(int level);
        void AddLeft(long value);
        void AddRight(long value);
        void Reduce();
        INode Add(INode other);
        INode Clone();
        string ToString();
    }
}
