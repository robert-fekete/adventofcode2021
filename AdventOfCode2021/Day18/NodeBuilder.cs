﻿using System;

namespace AdventOfCode2021.Day18
{
    class NodeBuilder
    {
        private long? value;
        private INode? left;
        private INode? right;

        public void AddLeftNode(INode node)
        {
            left = node;
        }

        public void AddRightNode(INode node)
        {
            right = node;
        }

        public void AddValue(long value)
        {
            this.value = value;
        }

        public INode Build()
        {
            if (value.HasValue)
            {
                return new SimpleNode(value.Value);
            }
            else if(left is not null && right is not null)
            {
                return new CompositeNode(left, right);
            }
            else
            {
                throw new InvalidOperationException("Values are null");
            }
        }

        public override string ToString()
        {
            return $"{left} - {right}";
        }
    }
}
