﻿using System;

namespace AdventOfCode2021.Day18
{
    class SimpleNode : INode
    {
        private long value;
        public SimpleNode(long value)
        {
            this.value = value;
        }

        public bool IsSimple => true;
        public long Magnitude => value;

        public ExplodeResult Explode(int _)
        {
            return ExplodeResult.WithoutExplode(this);
        }

        public void AddLeft(long value)
        {
            this.value += value;
        }

        public void AddRight(long value)
        {
            this.value += value;
        }

        public (bool, INode) Split()
        {
            if (value < 10)
            {
                return (false, this);
            }

            var left = (long)Math.Floor(value / 2.0);
            var right = (long)Math.Ceiling(value / 2.0);

            return (true, new CompositeNode(new SimpleNode(left), new SimpleNode(right)));
        }

        public void Reduce()
        {
            // Noop
        }

        public INode Add(INode other)
        {
            var newNode = new CompositeNode(this.Clone(), other.Clone());
            newNode.Reduce();
            return newNode;
        }

        public INode Clone()
        {
            return new SimpleNode(value);
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
