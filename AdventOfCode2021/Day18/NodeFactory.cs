﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day18
{
    class NodeFactory
    {
        public INode CreateNode(string input)
        {
            var history = new Stack<NodeBuilder>();
            var tokens = Tokenize(input);
            var currentBuilder = new NodeBuilder();

            foreach(var token in tokens)
            {
                if (token == "[")
                {
                    history.Push(currentBuilder);
                    currentBuilder = new NodeBuilder();
                }
                else if (token == "]")
                {
                    history.Peek().AddRightNode(currentBuilder.Build());
                    currentBuilder = history.Pop();
                }
                else if (token == ",")
                {
                    history.Peek().AddLeftNode(currentBuilder.Build());
                    currentBuilder = new NodeBuilder();
                }
                else
                {
                    var value = long.Parse(token);
                    currentBuilder.AddValue(value);
                }
            }

            return currentBuilder.Build();
        }

        private IEnumerable<string> Tokenize(string input)
        {
            var tokens = new List<string>();
            var buffer = new List<char>();

            foreach(var c in input)
            {
                if (IsSeparator(c))
                {
                    if (buffer.Any())
                    {
                        tokens.Add(string.Join("", buffer));
                        buffer.Clear();
                    }
                    tokens.Add(c.ToString());
                }
                else
                {
                    buffer.Add(c);
                }
            }

            return tokens;
        }

        private static bool IsSeparator(char c)
        {
            return c == '[' || c == ']' || c == ',';
        }
    }
}
