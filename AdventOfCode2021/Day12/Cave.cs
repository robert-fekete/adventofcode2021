﻿using System;

namespace AdventOfCode2021.Day12
{
    class Cave : IEquatable<Cave>
    {
        public readonly static Cave START = new Cave("start");
        public readonly static Cave END = new Cave("end");

        private readonly string name;

        public Cave(string name)
        {
            this.name = name;
        }

        public bool IsVisitable => !START.Equals(this);
        public bool IsBig => IsVisitable && char.IsUpper(name[0]);
        public bool IsSmall => IsVisitable && char.IsLower(name[0]);

        public override bool Equals(object? obj)
        {
            return Equals(obj as Cave);
        }

        public bool Equals(Cave? other)
        {
            return other != null &&
                   name == other.name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(name);
        }

        public override string ToString()
        {
            return name;
        }
    }
}
