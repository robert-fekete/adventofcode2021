﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day12
{
    class GraphBuilder
    {
        private readonly Dictionary<Cave, List<Cave>> neighbours = new Dictionary<Cave, List<Cave>>();

        public void Add(Cave a, Cave b)
        {
            AddNeighbour(a, b);
            AddNeighbour(b, a);
        }

        public Graph Build()
        {
            return new Graph(neighbours.ToDictionary<KeyValuePair<Cave, List<Cave>>, Cave, IReadOnlyCollection<Cave>>(kvp => kvp.Key, kvp => kvp.Value));
        }

        private void AddNeighbour(Cave a, Cave b)
        {
            if (!neighbours.ContainsKey(a))
            {
                neighbours[a] = new List<Cave>();
            }

            neighbours[a].Add(b);
        }
    }
}
