﻿using AdventOfCode.Framework;
using System.Collections.Generic;

namespace AdventOfCode2021.Day12
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "3510";

        public string SecondExpected => "122880";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "start-A",
                    "start-b",
                    "A-c",
                    "A-b",
                    "b-d",
                    "A-end",
                    "b-end",
                }, "10")
                .AddTestCase(new[]
                {
                    "dc-end",
                    "HN-start",
                    "start-kj",
                    "dc-start",
                    "dc-HN",
                    "LN-dc",
                    "HN-end",
                    "kj-sa",
                    "kj-HN",
                    "kj-dc",
                }, "19")
                .AddTestCase(new[]
                {
                    "fs-end",
                    "he-DX",
                    "fs-he",
                    "start-DX",
                    "pj-DX",
                    "end-zg",
                    "zg-sl",
                    "zg-pj",
                    "pj-he",
                    "RW-he",
                    "fs-DX",
                    "pj-RW",
                    "zg-RW",
                    "start-pj",
                    "he-WI",
                    "zg-he",
                    "pj-fs",
                    "start-RW",
                }, "226")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "start-A",
                    "start-b",
                    "A-c",
                    "A-b",
                    "b-d",
                    "A-end",
                    "b-end",
                }, "36")
                .AddTestCase(new[]
                {
                    "dc-end",
                    "HN-start",
                    "start-kj",
                    "dc-start",
                    "dc-HN",
                    "LN-dc",
                    "HN-end",
                    "kj-sa",
                    "kj-HN",
                    "kj-dc",
                }, "103")
                .AddTestCase(new[]
                {
                    "fs-end",
                    "he-DX",
                    "fs-he",
                    "start-DX",
                    "pj-DX",
                    "end-zg",
                    "zg-sl",
                    "zg-pj",
                    "pj-he",
                    "RW-he",
                    "fs-DX",
                    "pj-RW",
                    "zg-RW",
                    "start-pj",
                    "he-WI",
                    "zg-he",
                    "pj-fs",
                    "start-RW",
                }, "3509")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var graph = ParseGraph(input);
            var dfs = new DFS(graph);

            var paths = dfs.Explore(Cave.START, Cave.END, smallCaves => smallCaves == 0);

            return paths.Count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var graph = ParseGraph(input);
            var dfs = new DFS(graph);

            var paths = dfs.Explore(Cave.START, Cave.END, smallCaves => smallCaves <= 1);

            return paths.Count.ToString();
        }

        private static Graph ParseGraph(IEnumerable<string> input)
        {
            var builder = new GraphBuilder();

            foreach(var line in input)
            {
                var parts = line.Split("-");
                builder.Add(new Cave(parts[0]), new Cave(parts[1]));
            }

            return builder.Build();
        }
    }
}
