﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day12
{
    class Path
    {
        private readonly List<Cave> caves;
        private readonly Predicate<int> validityPredicate;

        private int smallCaveRepetition = 0;

        public Path(Cave start, Predicate<int> validityPredicate)
        {
            caves = new List<Cave>() { start };
            this.validityPredicate = validityPredicate;
        }

        public Path(Path other)
        {
            caves = new List<Cave>(other.caves);
            smallCaveRepetition = other.smallCaveRepetition;
            validityPredicate = other.validityPredicate;
        }

        public bool IsValid => validityPredicate(smallCaveRepetition);

        public void Add(Cave next)
        {
            if (next.IsSmall && caves.Contains(next))
            {
                smallCaveRepetition++;
            }

            caves.Add(next);
        }

        public bool Contains(Cave cave)
        {
            return caves.Contains(cave);
        }

        public override string ToString()
        {
            return string.Join(",", caves);
        }
    }
}
