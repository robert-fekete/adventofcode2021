﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day12
{
    internal class Graph
    {
        private readonly Dictionary<Cave, IReadOnlyCollection<Cave>> neighbours;

        public Graph(Dictionary<Cave, IReadOnlyCollection<Cave>> neighbours)
        {
            this.neighbours = neighbours;
        }

        internal IReadOnlyCollection<Cave> GetNeighbours(Cave cave)
        {
            return neighbours[cave];
        }
    }
}