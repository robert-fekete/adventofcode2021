﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day12
{
    class DFS
    {
        private readonly Graph graph;

        public DFS(Graph graph)
        {
            this.graph = graph;
        }

        public IReadOnlyCollection<Path> Explore(Cave start, Cave end, Predicate<int> pathValidityPredicate)
        {
            var backlog = new Stack<(Cave, Path)>();

            backlog.Push((start, new Path(start, pathValidityPredicate)));

            var paths = new List<Path>();
            while (backlog.Any())
            {
                (var cave, var path) = backlog.Pop();
                                
                if (cave.Equals(end))
                {
                    paths.Add(path);
                    continue;
                }

                foreach (var next in graph.GetNeighbours(cave))
                {
                    var nextPath = new Path(path);
                    nextPath.Add(next);
                    if (next.IsVisitable && nextPath.IsValid)
                    {
                        backlog.Push((next, nextPath));
                    }
                }
            }

            return paths;
        }
    }
}
