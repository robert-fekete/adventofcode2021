﻿namespace AdventOfCode2021.Day2
{
    interface ISubmarine
    {
        int Depth { get; }
        int Position { get; }

        void Down(int offset);
        void Forward(int offset);
        void Up(int offset);
    }
}