﻿using System;

namespace AdventOfCode2021.Day2
{
    class BasicSubmarine : ISubmarine
    {
        public int Position { get; private set; } = 0;
        public int Depth { get; private set; } = 0;

        public void Up(int offset)
        {
            Depth -= offset;
        }

        public void Down(int offset)
        {
            Depth += offset;
        }

        public void Forward(int offset)
        {
            Position += offset;
        }
    }
}
