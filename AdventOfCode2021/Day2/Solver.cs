﻿using AdventOfCode.Framework;
using System.Collections.Generic;

namespace AdventOfCode2021.Day2
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "1693300";

        public string SecondExpected => "1857958050";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "forward 5",
                    "down 5",
                    "forward 8",
                    "up 3",
                    "down 8",
                    "forward 2",
                }, "150")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "forward 5",
                    "down 5",
                    "forward 8",
                    "up 3",
                    "down 8",
                    "forward 2",
                }, "900")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var submarine = new BasicSubmarine();
            var result = Solve(input, submarine);

            return result.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var submarine = new AimedSubmarine();
            var result = Solve(input, submarine);

            return result.ToString();
        }

        private static int Solve(IEnumerable<string> input, ISubmarine submarine)
        {
            foreach (var line in input)
            {
                var parts = line.Split(' ');
                var offset = int.Parse(parts[1]);

                switch (parts[0])
                {
                    case "up":
                        submarine.Up(offset);
                        break;
                    case "down":
                        submarine.Down(offset);
                        break;
                    case "forward":
                        submarine.Forward(offset);
                        break;
                };
            }

            return submarine.Position * submarine.Depth;
        }
    }
}
