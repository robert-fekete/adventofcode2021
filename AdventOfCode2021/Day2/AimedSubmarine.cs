﻿namespace AdventOfCode2021.Day2
{
    class AimedSubmarine : ISubmarine
    {
        private int aim = 0;

        public int Depth { get; private set; } = 0;

        public int Position { get; private set; } = 0;

        public void Down(int offset)
        {
            aim += offset;
        }

        public void Up(int offset)
        {
            aim -= offset;
        }

        public void Forward(int offset)
        {
            Position += offset;
            Depth += aim * offset;
        }
    }
}
