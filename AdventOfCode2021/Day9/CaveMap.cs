﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day9
{
    class CaveMap
    {
        private readonly (int, int)[] deltas = new[] { (0, 1), (1, 0), (0, -1), (-1, 0) };
        private readonly int[][] map;

        public CaveMap(int[][] map)
        {
            this.map = map;
        }

        public int Height => map.Length;
        public int Width => map[0].Length;

        internal List<(int, int)> FindLowPoints()
        {
            var lowPoints = new List<(int, int)>();
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    var height = map[y][x];
                    var neighbours = GetNeighbours((x, y));
                    if (neighbours.All(p => map[p.Y][p.X] > height))
                    {
                        lowPoints.Add((x, y));
                    }
                }
            }

            return lowPoints;
        }

        internal int GetHeight((int X, int Y) p)
        {
            if (IsOutOfRange(p.X, Width) || IsOutOfRange(p.Y, Height))
            {
                throw new IndexOutOfRangeException();
            }

            return map[p.Y][p.X];
        }

        internal IReadOnlyCollection<(int X, int Y)> GetNeighbours((int X, int Y) p)
        {
            var neighbours = new List<(int, int)>();
            foreach((var dx, var dy) in deltas)
            {
                var x = p.X + dx;
                var y = p.Y + dy;
                if (IsOutOfRange(x, Width) || IsOutOfRange(y, Height))
                {
                    continue;
                }

                neighbours.Add((x, y));
            }

            return neighbours;
        }

        private bool IsOutOfRange(int value, int maxValue)
        {
            return value < 0 || value >= maxValue;
        }
    }
}
