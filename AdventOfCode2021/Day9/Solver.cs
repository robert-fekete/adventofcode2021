﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day9
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "500";

        public string SecondExpected => "970200";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "2199943210",
                    "3987894921",
                    "9856789892",
                    "8767896789",
                    "9899965678",
                }, "15")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "2199943210",
                    "3987894921",
                    "9856789892",
                    "8767896789",
                    "9899965678",
                }, "1134")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var map = ParseMap(input);

            var lowPoints = map.FindLowPoints();

            var totalRiskLevel = 0;
            foreach (var coordinates in lowPoints)
            {
                var height = map.GetHeight(coordinates);
                var riskLevel = height + 1;
                totalRiskLevel += riskLevel;
            }

            return totalRiskLevel.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var map = ParseMap(input);
            var dfs = new DFS(map);

            var lowPoints = map.FindLowPoints();

            var basins = new List<int>();
            foreach (var coordinates in lowPoints)
            {
                var basinSize = dfs.Traverse(coordinates);
                basins.Add(basinSize);
            }

            var result = basins.OrderByDescending(n => n).Take(3).Aggregate(1, (acc, val) => acc * val);
            return result.ToString();
        }

        private static CaveMap ParseMap(IEnumerable<string> input)
        {
            var heights = input.Select(line => line.Select(c => c - '0').ToArray()).ToArray();
            var map = new CaveMap(heights);
            return map;
        }
    }
}
