﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2021.Day9
{
    class DFS
    {
        private readonly CaveMap map;

        public DFS(CaveMap map)
        {
            this.map = map;
        }

        public int Traverse((int X, int Y) startingPoint)
        {
            var backlog = new Stack<(int, int)>();
            var visited = new HashSet<(int, int)>();

            backlog.Push(startingPoint);

            var score = 0;
            while (backlog.Any())
            {
                var currentPoint = backlog.Pop();

                if (visited.Contains(currentPoint))
                {
                    continue;
                }
                visited.Add(currentPoint);

                score++;

                foreach(var next in map.GetNeighbours(currentPoint))
                {
                    if (map.GetHeight(next) == 9)
                    {
                        continue;
                    }

                    backlog.Push(next);
                }
            }

            return score;
        }
    }
}
