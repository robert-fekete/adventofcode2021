﻿namespace AdventOfCode2021.Day23
{
    struct Amphipod
    {
        public char type;
        public byte state;

        public override string ToString()
        {
            return $"({type}, {state})";
        }
    }
}
