﻿inp_s = """#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########"""

inp_b = """"""

is_big = False

if is_big:
    inp = inp_b
else:
    inp = inp_s

costs = {
    'A' : 1,
    'B' : 10,
    'C' : 100,
    'D' : 1000,
}

class Pod:
   
    def __init__(self, letter, id):
        self.letter = letter
        self.id = id
        self.is_moved = False
        self.is_finished = False
        self.cost = costs[letter]
   
    def copy_as_moved(self):
       
        new = Pod(self.letter, self.id)
        new.is_moved = True
       
        return new
   
    def copy_as_finished(self):
       
        new = Pod(self.letter, self.id)
        new.is_moved = self.is_moved
        new.is_finished = True
       
        return new
       
    def __str__(self):
       
        return f"{self.letter} - {self.id} - {self.is_moved}"

def create_graph(size):
    edges = {
        0: [1],
        1: [0, 2, 7],
        2: [1, 7, 8, 3],
        3: [2, 4, 8, 9],
        4: [3, 5, 9, 10],
        5: [4, 10, 6],
        6: [5],
        7: [1, 2, 11],
        8: [2, 3, 12],
        9: [3, 4, 13],
        10: [4, 5, 14],
        11: [7],
        12: [8],
        13: [9],
        14: [10],
    }
   
    if size != 2:
        edges[7] = [1, 2, 11]
        edges[8] = [2, 3, 12]
        edges[9] = [3, 4, 13]
        edges[10] = [4, 5, 14]
       
        edges[11] = [7, 15]
        edges[12] = [8, 16]
        edges[13] = [9, 17]
        edges[14] = [10, 18]
       
        edges[15] = [11, 19]
        edges[16] = [12, 20]
        edges[17] = [13, 21]
        edges[18] = [14, 22]
       
        edges[19] = [15]
        edges[20] = [16]
        edges[21] = [17]
        edges[22] = [18]
   
    return edges
   
def get_dependencies(graph):
   
    dependencies = {}
    for k in graph:
        dependencies[k]= get_paths(graph, k)
       
    return dependencies
   
def get_paths(graph, root):
   
    backlog = [(root, [])]
   
    visited = set()
    paths = {}
    while backlog:
        (current, path) = backlog.pop(0)
       
        if current in visited:
            continue
       
        visited.add(current)
       
        if current != root:
            paths[current] = tuple(path)
           
        for next in graph[current]:
            new_path = path[:]
            new_path.append(next)
            backlog.append((next, new_path))
           
    return paths
   
def get_pods(input, size, target_positions):
   
    starting = []
    for i in range(7):
        starting.append(EMPTY)
       
    for y in range(2, 2 + size):
        for x in range(3, 10, 2):
           
            state = 0
            if len(starting) == target_positions[input[y][x]][0]:
                state = 2
            starting.append((input[y][x], state))
           
           
    return tuple(starting)
   
def get_target_positions(size):
   
    targets = {}
    if size == 2:
        targets['A'] = [11, 7]
        targets['B'] = [12, 8]
        targets['C'] = [13, 9]
        targets['D'] = [14, 10]
    else:
        raise "SHIT"
        targets['A'] = [7, 11, 15, 19]
        targets['B'] = [8, 12, 16, 20]
        targets['C'] = [9, 13, 17, 21]
        targets['D'] = [10, 14, 18, 22]
       
    return targets

def is_finished(pos, target_positions):
   
    for i in range(len(pos)):
        if pos[i] == EMPTY:
            continue
        else:
            if i not in target_positions[pos[i][0]]:

                return False
               
    return True

import heapq

#01 2 3 4 56
#...........
#  7 8 9 A  A=10,
#  B C D E  B=11,C=12,D=13,E=14
#  F G H I  F=15,G=16,H=17,I=18
#  J K L M  J=19,K=20,L=21,M=22

EMPTY = tuple('Z')
def solve(inp):
   
   
    double_cost_nodes = [1, 2, 3, 4, 5, 7, 8, 9, 10]
    target_positions = get_target_positions(2)
    lines = inp.split("\n")
    graph = create_graph(2)
    dependencies = get_dependencies(graph)
    pods = get_pods(lines, 2, target_positions)
   
    backlog = []
    heapq.heapify(backlog)
    heapq.heappush(backlog, (0, pods))
   
    visited = set()
    while backlog:
       
        (cost, current) = heapq.heappop(backlog)
       
        if sum([1 if c != EMPTY else 0 for c in current[:7]]) > 3:
            continue
       
        if current in visited:
            continue
        visited.add(current)
       
        #print(cost)
        if cost == (12521 + 3023):
            print(current)
       
        if is_finished(current, target_positions):


            return cost
           
        for i in range(len(current)):
            if current[i] != EMPTY:
                possible_moves = []
                state = 0
                if current[i][1] == 0:
                    for ind in graph:
                        if current[ind] != EMPTY:
                            continue
                        else:
                            if ind < 7:
                                possible_moves.append(ind)
                            elif ind in target_positions[current[i][0]]:
                                slot = -1
                                for n in target_positions[current[i][0]]:
                                    if current[n] == EMPTY:
                                        slot = n
                                        break
                                    elif current[n][0] != current[i][0]:
                                        break
                                    else:
                                        continue
                                if slot == i:
                                    possible_moves.append(i)
                               
                    possible_moves = [ind for ind in graph if current[ind] == EMPTY and (ind < 7 or ind in target_positions[current[i][0]])]
                elif current[i][1] == 1:
                    next = -1
                   
                    if any([current[c] != EMPTY and current[c][0] != current[i][0]] for c in target_positions[current[i][0]]):
                        possible_moves = []
                   
                    else:
                        for n in target_positions[current[i][0]]:
                            if current[n] == EMPTY:
                                next = n
                                break
                        if next == -1:
                            #target positions are not free
                            pass
                            raise "How"
                        else:
                            possible_moves = [next]
                else:
                    continue
               
                for next in possible_moves:
                    path = dependencies[i][next]
                    if all([current[c] == EMPTY for c in path]):
                        next_cost = 0
                        for c in path:
                            if c in double_cost_nodes:
                                next_cost += 2
                            else:
                                next_cost += 1
                        next_cost *= costs[current[i][0]]
                        next_pos = list(current)
                       
                        state = 1
                        if next in target_positions[current[i][0]]:
                            state = 2
                       
                        next_pos[next] = (current[i][0], state)
                        next_pos[i] = EMPTY
                       
                        heapq.heappush(backlog, (cost + next_cost, tuple(next_pos)))
   
    raise "Who dis"
       
print(solve(inp))

