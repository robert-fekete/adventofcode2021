﻿using AdventOfCode.Framework;
using AdventOfCode2021.Day15;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day23
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "15538";

        public string SecondExpected => "47258";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "#############",
                    "#...........#",
                    "###B#C#B#D###",
                    "  #A#D#C#A#",
                    "  #########",
                }, "12521")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "#############",
                    "#...........#",
                    "###B#C#B#D###",
                    "  #A#D#C#A#",
                    "  #########",
                }, "44169")
                .AddTest(ExecuteSecond)
                .Build();
        }

        private readonly static Amphipod EMPTY = new Amphipod();
        public string ExecuteFirst(IEnumerable<string> input)
        {
            var cost = Solve(input);

            return cost.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            Console.WriteLine("Real slow, might need some improvements");

            var foldedPart = new[] { "  #D#C#B#A#", "  #D#B#A#C#" };
            var unfoldedInput = new List<string>();
            unfoldedInput.AddRange(input.Take(3));
            unfoldedInput.AddRange(foldedPart);
            unfoldedInput.AddRange(input.Skip(3));

            var report = new Report();
            report.Start();
            var cost = Solve(unfoldedInput);
            report.Stop();
            report.Print();

            return cost.ToString();
        }

        private static long Solve(IEnumerable<string> input)
        {
            var isSimple = input.Count() == 5;
            var costs = GetCost();
            var graph = GetGraph(isSimple);
            var dependencies = GetDependencies(graph);
            var targetPositions = GetTargetPositions(isSimple);
            var amphipods = ParseInput(input, targetPositions);

            var backlog = new Heap<Amphipod[]>();
            backlog.Add(amphipods, 0 + H(amphipods, targetPositions, costs, graph, dependencies));

            var visited = new HashSet<Amphipod[]>();

            int tolarence = 500;
            long best = long.MaxValue;

            while (!backlog.IsEmpty())
            {
                (var current, var fakeCost) = backlog.Pop();
                var cost = fakeCost - H(current, targetPositions, costs, graph, dependencies);

                //Console.WriteLine(fakeCost);

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (IsFinished(current, targetPositions))
                {
                    tolarence--;
                    if (tolarence == 0)
                    {
                        return best;
                    }
                    if (cost < best)
                    {
                        best = cost;
                    }

                    //return cost;
                }

                var didClose = false;

                for (int i = 0; i < current.Length; i++)
                {
                    if (!current[i].Equals(EMPTY) && current[i].state != 2)
                    {
                        var target = GetFirstEmptyTarget(targetPositions[current[i].type], current, current[i].type);
                        if (target is not null)
                        {
                            var next = target.Value;
                            var path = dependencies[i][next];
                            if (path.All(p => current[p].Equals(EMPTY)))
                            {
                                didClose = true;
                                (var nextPosition, var nextCost) = GetNextStep(graph, targetPositions, current, costs[current[i].type], i, next, path);
                                backlog.Add(nextPosition, cost + nextCost + H(nextPosition, targetPositions, costs, graph, dependencies));
                            }
                        }
                    }
                }

                if (didClose)
                {
                    continue;
                }

                for (int i = 0; i < current.Length; i++)
                {
                    if (!current[i].Equals(EMPTY) && current[i].state != 2)
                    {
                        var possibleMoves = new List<int>();

                        var target = GetFirstEmptyTarget(targetPositions[current[i].type], current, current[i].type);
                        if (target is not null)
                        {
                            possibleMoves.Add(target.Value);
                        }
                        if(i >= 7)
                        {
                            for(int n = 0; n < 7; n++)
                            {
                                if (current[n].Equals(EMPTY))
                                {
                                    possibleMoves.Add(n);
                                }
                            }
                        }

                        foreach(var next in possibleMoves)
                        {
                            var path = dependencies[i][next];
                            if (path.All(p => current[p].Equals(EMPTY)))
                            {
                                (var nextPosition, var nextCost) = GetNextStep(graph, targetPositions, current, costs[current[i].type], i, next, path);
                                backlog.Add(nextPosition, cost + nextCost + H(nextPosition, targetPositions, costs, graph, dependencies));
                            }
                        }
                    }
                }
            }

            throw new InvalidOperationException("Couldn't find a solution");
        }

        private static long H(Amphipod[] current, Dictionary<char, int[]> targetPositions, Dictionary<char, int> costs, Dictionary<int, Dictionary<int, int>> graph, Dictionary<int, Dictionary<int, int[]>> dependencies)
        {
            var h = 0L;
            var typesToMove = new Dictionary<char, int>()
            {
                {'A', 0 },
                {'B', 0 },
                {'C', 0 },
                {'D', 0 },
            };

            for(int i = 0; i < current.Length; i++)
            {
                var amphipod = current[i];
                if (amphipod.Equals(EMPTY))
                {
                    continue;
                }
                if (amphipod.state < 2)
                {
                    if (targetPositions[amphipod.type].Contains(i))
                    {
                        foreach(var pos in targetPositions[amphipod.type])
                        {
                            if (pos == i)
                            {
                                break;
                            }
                            if (current[pos].type == current[i].type)
                            {
                                continue;
                            }
                            else
                            {
                                var target = targetPositions[amphipod.type].Last();
                                var closestHallway = target - 6;

                                var path = dependencies[i][closestHallway];
                                var cost = CalculateCost(i, path, costs[amphipod.type], graph);
                                h += cost;

                                path = dependencies[closestHallway][target];
                                cost = CalculateCost(closestHallway, path, costs[amphipod.type], graph);
                                h += cost;

                                typesToMove[amphipod.type]++;
                            }
                        }
                    }
                    else
                    {
                        var target = targetPositions[amphipod.type].Last();
                        if (target != i)
                        {
                            var path = dependencies[i][target];
                            var cost = CalculateCost(i, path, costs[amphipod.type], graph);
                            h += cost;

                            typesToMove[amphipod.type]++;
                        }
                    }
                }
            }

            foreach(var kvp in typesToMove)
            {
                h += (kvp.Value * (kvp.Value + 1) / 2) * costs[kvp.Key];
            }

            return h;
        }

        private static (Amphipod[], long) GetNextStep(Dictionary<int, Dictionary<int, int>> graph, Dictionary<char, int[]> targetPositions, Amphipod[] current, int cost, int currentPosition, int next, int[] path)
        {
            var nextCost = CalculateCost(currentPosition, path, cost, graph);
            var nextPosition = current.ToArray();

            byte nextState = 1;
            if (targetPositions[current[currentPosition].type].Contains(next))
            {
                nextState = 2;
            }

            nextPosition[next] = new Amphipod()
            {
                type = current[currentPosition].type,
                state = nextState
            };
            nextPosition[currentPosition] = EMPTY;

            return (nextPosition, nextCost);
        }

        private static int? GetFirstEmptyTarget(int[] targetPositions, Amphipod[] currentState, char type)
        {
            foreach (var pos in targetPositions)
            {
                if (currentState[pos].Equals(EMPTY))
                {
                    return pos;
                }
                else
                {
                    if (currentState[pos].type == type)
                    {
                        continue;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            throw new InvalidOperationException("How");
        }

        private static long CalculateCost(int startingPosition, int[] path, int cost, Dictionary<int, Dictionary<int, int>> graph)
        {
            var steps = 0L;
            var prev = startingPosition;
            for (int i = 0; i < path.Length; i++)
            {
                steps += graph[prev][path[i]];
                prev = path[i];
            }

            return steps * cost;
        }

        private static bool IsFinished(Amphipod[] state, Dictionary<char, int[]> targetPositions)
        {
            var totalTargets = targetPositions.Sum(kvp => kvp.Value.Length);
            for(int i = 7; i < 7 + totalTargets; i++)
            {
                var type = state[i].type;
                if (state[i].Equals(EMPTY) || !targetPositions[type].Contains(i))
                {
                    return false;
                }
            }

            return true;
        }

        private static Amphipod[] ParseInput(IEnumerable<string> input, Dictionary<char, int[]> targetPositions)
        {
            var lines = input.ToArray();
            var amphipods = new List<Amphipod>();

            for(int i = 0; i < 7; i++)
            {
                amphipods.Add(EMPTY);
            }

            for(int y = 2; y < lines.Length - 1; y++)
            {
                for (int x = 3; x < 10; x += 2)
                {
                    var position = amphipods.Count;
                    var type = lines[y][x];
                    var state = (byte) (targetPositions[type].First() == position ? 2 : 0);
                    amphipods.Add(new Amphipod()
                    {
                        type = type,
                        state = state
                    });
                }
            }

            return amphipods.ToArray();
        }

        private static Dictionary<char, int[]> GetTargetPositions(bool simple)
        {
            /// ###############
            /// # 01 2 3 4 56 #
            /// # ........... #
            /// ### 7 8 9 A ### A=10,
            ///   # B C D E #   B=11,C=12,D=13,E=14
            ///   # F G H I #   F=15,G=16,H=17,I=18
            ///   # J K L M #   J=19,K=20,L=21,M=22
            ///   ###########
            
            if (simple)
            {
                return new Dictionary<char, int[]>()
                {
                    { 'A', new []{11, 7} },
                    { 'B', new []{12, 8} },
                    { 'C', new []{13, 9} },
                    { 'D', new []{14, 10} },
                };
            }
            else
            {
                return new Dictionary<char, int[]>()
                {
                    { 'A', new []{19, 15, 11, 7} },
                    { 'B', new []{20, 16, 12, 8} },
                    { 'C', new []{21, 17, 13, 9} },
                    { 'D', new []{22, 18, 14, 10} },
                };
            }
        }

        private static Dictionary<int, Dictionary<int, int>> GetGraph(bool simple)
        {
            var graph = new Dictionary<int, Dictionary<int, int>>()
            {
                { 0, new Dictionary<int, int>() { { 1, 1} } },
                { 1, new Dictionary<int, int>() { { 0, 1}, {2, 2}, { 7, 2 } } },
                { 2, new Dictionary<int, int>() { { 1, 2 }, {3, 2 }, { 7, 2 }, { 8, 2 } } },
                { 3, new Dictionary<int, int>() { { 2, 2 }, { 4, 2 }, { 8, 2 }, { 9, 2 } } },
                { 4, new Dictionary<int, int>() { { 3, 2 }, { 5, 2 }, { 9, 2 }, { 10, 2 } } },
                { 5, new Dictionary<int, int>() { { 4, 2 }, { 10, 2 }, { 6, 1 } } },
                { 6, new Dictionary<int, int>() { { 5, 1 } } },
                { 7, new Dictionary<int, int>() { { 1, 2 }, { 2, 2 }, { 11, 1 } } },
                { 8, new Dictionary<int, int>() { { 2, 2 }, { 3, 2 }, { 12, 1 } } },
                { 9, new Dictionary<int, int>() { { 3, 2 }, { 4, 2 }, { 13, 1 } } },
                { 10, new Dictionary<int, int>() { { 4, 2 }, { 5, 2 }, { 14, 1 } } },
                { 11, new Dictionary<int, int>() { { 7, 1 } } },
                { 12, new Dictionary<int, int>() { { 8, 1} } },
                { 13, new Dictionary<int, int>() { { 9, 1} } },
                { 14, new Dictionary<int, int>() { { 10, 1} } },
            };

            if (!simple)
            {
                graph[11] = new Dictionary<int, int>() { { 7, 1 }, { 15, 1 } };
                graph[12] = new Dictionary<int, int>() { { 8, 1 }, { 16, 1 } };
                graph[13] = new Dictionary<int, int>() { { 9, 1 }, { 17, 1 } };
                graph[14] = new Dictionary<int, int>() { { 10, 1 }, { 18, 1 } };

                graph[15] = new Dictionary<int, int>() { { 11, 1 }, { 19, 1 } };
                graph[16] = new Dictionary<int, int>() { { 12, 1 }, { 20, 1 } };
                graph[17] = new Dictionary<int, int>() { { 13, 1 }, { 21, 1 } };
                graph[18] = new Dictionary<int, int>() { { 14, 1 }, { 22, 1 } };

                graph[19] = new Dictionary<int, int>() { { 15, 1 } };
                graph[20] = new Dictionary<int, int>() { { 16, 1 } };
                graph[21] = new Dictionary<int, int>() { { 17, 1 } };
                graph[22] = new Dictionary<int, int>() { { 18, 1 } };
            }

            return graph;
        }

        private static Dictionary<char, int> GetCost()
        {
            return new Dictionary<char, int>()
            {
                { 'A', 1 },
                { 'B', 10 },
                { 'C', 100 },
                { 'D', 1000 },
            };
        }

        private static Dictionary<int, Dictionary<int, int[]>> GetDependencies(Dictionary<int, Dictionary<int, int>> graph)
        {
            var dependencies = new Dictionary<int, Dictionary<int, int[]>>();

            foreach(var key in graph.Keys)
            {
                dependencies[key] = GetPaths(graph, key);
            }

            return dependencies;
        }

        private static Dictionary<int, int[]> GetPaths(Dictionary<int, Dictionary<int, int>> graph, int root)
        {
            var backlog = new Queue<(int, List<int>)>();
            backlog.Enqueue((root, new List<int>()));

            var visited = new HashSet<int>();

            var paths = new Dictionary<int, int[]>();
            while (backlog.Any())
            {
                (var current, var path) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (current != root)
                {
                    paths[current] = path.ToArray();
                }

                foreach(var next in graph[current].Keys)
                {
                    if (!visited.Contains(next))
                    {
                        var newPath = path.ToList();
                        newPath.Add(next);
                        backlog.Enqueue((next, newPath));
                    }
                }
            }

            return paths;
        }
    }
}
