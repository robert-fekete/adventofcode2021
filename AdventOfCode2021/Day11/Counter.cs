﻿namespace AdventOfCode2021.Day11
{
    class Counter
    {
        public int Value { get; private set; } = 0;

        public void Increament()
        {
            Value++;
        }
    }
}
