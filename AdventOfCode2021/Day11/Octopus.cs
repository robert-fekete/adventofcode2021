﻿using System;

namespace AdventOfCode2021.Day11
{
    class Octopus
    {
        private int energy;
        private readonly Action callback;

        public Octopus(int initialEnergy, Action callback)
        {
            energy = initialEnergy;
            this.callback = callback;
        }

        public bool IsFlashed { get; private set; }
        public bool IsCharged => energy > 9;

        internal void Flash()
        {
            IsFlashed = true;
            callback();
        }

        internal void IncreaseEnergy()
        {
            energy++;
        }

        internal void ResetEnergy()
        {
            if (IsCharged)
            {
                energy = 0;
            }
            IsFlashed = false;
        }

        public override string ToString()
        {
            return energy.ToString();
        }
    }
}
