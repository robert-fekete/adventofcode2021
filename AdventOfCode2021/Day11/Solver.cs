﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day11
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "1735";

        public string SecondExpected => "400";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "5483143223",
                    "2745854711",
                    "5264556173",
                    "6141336146",
                    "6357385478",
                    "4167524645",
                    "2176841721",
                    "6882881134",
                    "4846848554",
                    "5283751526",
                }, "1656")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "5483143223",
                    "2745854711",
                    "5264556173",
                    "6141336146",
                    "6357385478",
                    "4167524645",
                    "2176841721",
                    "6882881134",
                    "4846848554",
                    "5283751526",
                }, "195")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var counter = new Counter();
            var swarm = ParseOctopi(input, counter);

            for (int i = 0; i < 100; i++)
            {
                swarm.Step();
            }

            return counter.Value.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var counter = new Counter();
            var swarm = ParseOctopi(input, counter);

            for (int gen = 1; gen < 1000; gen++)
            {
                swarm.Step();
                if (swarm.IsSynchronized)
                {
                    return gen.ToString();
                }
            }

            throw new InvalidOperationException("No synchronication was found");
        }

        private static OctopusSwarm ParseOctopi(IEnumerable<string> input, Counter counter)
        {
            var octopi = input.Select(l => l.Select(c => new Octopus(c - '0', () => counter.Increament())).ToArray()).ToArray();

            var graph = new Graph(octopi);
            return new OctopusSwarm(graph);
        }
    }
}
