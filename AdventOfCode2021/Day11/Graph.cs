﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2021.Day11
{
    class Graph
    {
        private readonly (int dx, int dy)[] deltas = new[] { (-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1) };
        private readonly Octopus[][] octopi;

        public Graph(Octopus[][] octopi)
        {
            this.octopi = octopi;
        }

        public IReadOnlyCollection<(int X, int Y)> GetNeighbours((int X, int Y) coordinates)
        {
            var neighbours = new List<(int, int)>();
            foreach ((var dx, var dy) in deltas)
            {
                var x = coordinates.X + dx;
                var y = coordinates.Y + dy;

                if ((y >= 0 && y < octopi.Length) && (x >= 0 && x < octopi[y].Length))
                {
                    neighbours.Add((x, y));
                }
            }

            return neighbours;
        }

        public Octopus Get((int X, int Y) coordinates)
        {
            return octopi[coordinates.Y][coordinates.X];
        }

        public IEnumerable<(int, int)> Find(Predicate<Octopus> predicate)
        {
            for(int y = 0; y < octopi.Length; y++)
            {
                for(int x = 0; x < octopi[y].Length; x++)
                {
                    if (predicate(octopi[y][x]))
                    {
                        yield return (x, y);
                    }
                }
            }
        }

        public void ExecuteForAll(Action<Octopus> action)
        {
            foreach (var line in octopi)
            {
                foreach (var octopus in line)
                {
                    action(octopus);
                }
            }
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            for (var y = 0; y < octopi.Length; y++)
            {
                for (var x = 0; x < octopi[y].Length; x++)
                {
                    builder.Append(octopi[y][x]);
                }
                builder.AppendLine();
            }

            return builder.ToString();
        }
    }
}
