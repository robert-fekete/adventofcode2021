﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day11
{
    class OctopusSwarm
    {
        private readonly Graph octopi;

        public OctopusSwarm(Graph octopi)
        {
            this.octopi = octopi;
        }

        public bool IsSynchronized => !octopi.Find(o => !o.IsFlashed).Any();

        public void Step()
        {
            octopi.ExecuteForAll(o => o.ResetEnergy());

            octopi.ExecuteForAll(o => o.IncreaseEnergy());

            ExecuteFlash();
        }

        private void ExecuteFlash()
        {
            var chargedOctopi = octopi.Find(o => o.IsCharged);
            var backlog = new Stack<(int, int)>(chargedOctopi);
            
            while (backlog.Any())
            {
                var coordinates = backlog.Pop();
                var octopus = octopi.Get(coordinates);

                if (octopus.IsCharged && !octopus.IsFlashed)
                {
                    octopus.Flash();
                    foreach(var next in octopi.GetNeighbours(coordinates))
                    {
                        var neighbour = octopi.Get(next);
                        neighbour.IncreaseEnergy();
                        backlog.Push(next);
                    }
                }
            }
        }

        public override string ToString()
        {
            return octopi.ToString();
        }
    }
}
