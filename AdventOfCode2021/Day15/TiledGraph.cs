﻿using System.Collections.Generic;

namespace AdventOfCode2021.Day15
{
    internal class TiledGraph : IGraph
    {
        private readonly (int, int)[] deltas = new (int, int)[] { (1, 0), (0, 1), (-1, 0), (0, -1) };
        private readonly int[][] nodes;

        public TiledGraph(int[][] nodes)
        {
            this.nodes = nodes;
        }

        public (int, int) Start => (0, 0);
        public (int, int) End => (Width - 1, Height - 1);
        public int Size => Width * Height;
        private int Width => nodes[0].Length * 5;
        private int Height => nodes.Length * 5;

        public IEnumerable<((int X, int Y), int Cost)> GetNeighbours((int X, int Y) coordinates)
        {
            var neighbours = new List<((int, int), int)>();
            foreach ((var dx, var dy) in deltas)
            {
                var x = coordinates.X + dx;
                var y = coordinates.Y + dy;

                if (InBounds((x, y)))
                {
                    neighbours.Add(((x, y), GetCost((x, y))));
                }
            }

            return neighbours;
        }

        private int GetCost((int X, int Y) coordinates)
        {
            var additionX = coordinates.X / nodes[0].Length;
            var x = coordinates.X % nodes[0].Length;

            var additionY = coordinates.Y / nodes.Length;
            var y = coordinates.Y % nodes.Length;

            var cost = nodes[y][x] + additionX + additionY;
            if (cost > 9)
            {
                cost -= 9;
            }

            return cost;
        }

        private bool InBounds((int X, int Y) coordinates)
        {
            return coordinates.Y >= 0 && coordinates.Y < Height
                && coordinates.X >= 0 && coordinates.X < Width;
        }
    }
}