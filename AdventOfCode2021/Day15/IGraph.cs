﻿using System.Collections.Generic;

namespace AdventOfCode2021.Day15
{
    interface IGraph
    {
        (int, int) End { get; }
        (int, int) Start { get; }
        int Size { get; }

        IEnumerable<((int X, int Y), int Cost)> GetNeighbours((int X, int Y) coordinates);
    }
}