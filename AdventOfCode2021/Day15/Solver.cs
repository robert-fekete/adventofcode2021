﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day15
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "458";

        public string SecondExpected => "2800";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "1163751742",
                    "1381373672",
                    "2136511328",
                    "3694931569",
                    "7463417111",
                    "1319128137",
                    "1359912421",
                    "3125421639",
                    "1293138521",
                    "2311944581",
                }, "40")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "1163751742",
                    "1381373672",
                    "2136511328",
                    "3694931569",
                    "7463417111",
                    "1319128137",
                    "1359912421",
                    "3125421639",
                    "1293138521",
                    "2311944581",
                }, "315")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var graph = ParseGraph(input, true);
            var dijkstra = new Dijkstra(graph);

            var cost = dijkstra.FindShortestPath(graph.Start, graph.End);

            return cost.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var graph = ParseGraph(input, false);
            var dijkstra = new Dijkstra(graph);

            var cost = dijkstra.FindShortestPath(graph.Start, graph.End);

            return cost.ToString();
        }

        private IGraph ParseGraph(IEnumerable<string> input, bool isFirst)
        {
            var nodes = input.Select(l => l.Select(c => c - '0').ToArray()).ToArray();

            if (isFirst)
            {
                return new Graph(nodes);
            }
            else
            {
                return new TiledGraph(nodes);
            }
        }
    }
}
