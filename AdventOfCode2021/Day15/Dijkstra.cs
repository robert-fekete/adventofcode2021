﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day15
{
    class Dijkstra
    {
        private readonly IGraph graph;

        public Dijkstra(IGraph graph)
        {
            this.graph = graph;
        }

        public long FindShortestPath((int, int) start, (int, int) end)
        {
            var distances = new Dictionary<(int, int), long>();
            distances[start] = 0;

            var backlog = new Heap<(int, int)>(graph.Size);
            backlog.Add(start, 0);

            var visited = new HashSet<(int, int)>();

            while (!backlog.IsEmpty())
            {
                (var current, var totalDistance) = backlog.Pop();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (current == end)
                {
                    return totalDistance;
                }

                foreach((var next, var cost) in graph.GetNeighbours(current))
                {
                    if (visited.Contains(next))
                    {
                        continue;
                    }

                    var currentCost = distances.ContainsKey(next) ? distances[next] : long.MaxValue;
                    var newCost = distances[current] + cost;

                    if (newCost < currentCost)
                    {
                        distances[next] = newCost;
                        backlog.Add(next, newCost);
                    }

                }
            }

            throw new InvalidOperationException("Couldn't find target");
        }
    }
}
