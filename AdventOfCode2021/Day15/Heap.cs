﻿using System;

namespace AdventOfCode2021.Day15
{
    internal class Heap<T>
    {
        private (T Element, long Weight)[] elements;
        private int currentSize;

        public Heap(int size = 10)
        {
            elements = new (T, long)[size];
        }

        private int GetLeftChildIndex(int elementIndex) => 2 * elementIndex + 1;
        private int GetRightChildIndex(int elementIndex) => 2 * elementIndex + 2;
        private int GetParentIndex(int elementIndex) => (elementIndex - 1) / 2;

        private bool HasLeftChild(int elementIndex) => GetLeftChildIndex(elementIndex) < currentSize;
        private bool HasRightChild(int elementIndex) => GetRightChildIndex(elementIndex) < currentSize;
        private bool IsRoot(int elementIndex) => elementIndex == 0;

        private (T Element, long Weight) GetLeftChild(int elementIndex) => elements[GetLeftChildIndex(elementIndex)];
        private (T Element, long Weight) GetRightChild(int elementIndex) => elements[GetRightChildIndex(elementIndex)];
        private (T Element, long Weight) GetParent(int elementIndex) => elements[GetParentIndex(elementIndex)];

        private void Swap(int firstIndex, int secondIndex)
        {
            var temp = elements[firstIndex];
            elements[firstIndex] = elements[secondIndex];
            elements[secondIndex] = temp;
        }

        public bool IsEmpty()
        {
            return currentSize == 0;
        }

        public (T, long) Peek()
        {
            if (currentSize == 0)
                throw new IndexOutOfRangeException();

            return elements[0];
        }

        public (T, long) Pop()
        {
            if (currentSize == 0)
            {
                throw new IndexOutOfRangeException();
            }

            var result = elements[0];
            elements[0] = elements[currentSize - 1];
            currentSize--;

            ReCalculateDown();

            return result;
        }

        public void Add(T element, long weight)
        {
            if (currentSize == elements.Length)
            {
                var newArray = new (T, long)[elements.Length * 2];
                for (int i = 0; i < elements.Length; i++)
                {
                    newArray[i] = elements[i];
                }
                elements = newArray;
            }

            elements[currentSize] = (element, weight);
            currentSize++;

            ReCalculateUp();
        }

        private void ReCalculateDown()
        {
            int index = 0;
            while (HasLeftChild(index))
            {
                var smallerIndex = GetLeftChildIndex(index);
                if (HasRightChild(index) && GetRightChild(index).Weight < GetLeftChild(index).Weight)
                {
                    smallerIndex = GetRightChildIndex(index);
                }

                if (elements[smallerIndex].Weight >= elements[index].Weight)
                {
                    break;
                }

                Swap(smallerIndex, index);
                index = smallerIndex;
            }
        }

        private void ReCalculateUp()
        {
            var index = currentSize - 1;
            while (!IsRoot(index) && elements[index].Weight < GetParent(index).Weight)
            {
                var parentIndex = GetParentIndex(index);
                Swap(parentIndex, index);
                index = parentIndex;
            }
        }
    }
}
