﻿using System.Collections.Generic;

namespace AdventOfCode2021.Day15
{
    class Graph : IGraph
    {
        private readonly (int, int)[] deltas = new (int, int)[] { (1, 0), (-1, 0), (0, 1), (0, -1) };
        private readonly int[][] nodes;

        public Graph(int[][] nodes)
        {
            this.nodes = nodes;
        }

        public (int, int) Start => (0, 0);
        public (int, int) End => (nodes[0].Length - 1, nodes.Length - 1);
        public int Size => nodes.Length * nodes[0].Length;

        public IEnumerable<((int X, int Y), int Cost)> GetNeighbours((int X, int Y) coordinates)
        {
            var neighbours = new List<((int, int), int)>();
            foreach ((var dx, var dy) in deltas)
            {
                var x = coordinates.X + dx;
                var y = coordinates.Y + dy;

                if (InBounds((x, y)))
                {
                    neighbours.Add(((x, y), nodes[y][x]));
                }
            }

            return neighbours;
        }

        private bool InBounds((int X, int Y) coordinates)
        {
            return coordinates.Y >= 0 && coordinates.Y < nodes.Length
                && coordinates.X >= 0 && coordinates.X < nodes[coordinates.Y].Length;
        }
    }
}
