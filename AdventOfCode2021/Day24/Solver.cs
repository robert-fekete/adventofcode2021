﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day24
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "51939397989999";

        public string SecondExpected => "11717131211195";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var arguments = ParseArguments(input);

            var rules = GenerateRules(arguments);

            var monad = new Monad(arguments);
            var generator = new ModelNumberGenerator(rules);

            var modelNumber = generator.GenerateLargest(14);

            if(!monad.ValidateNumber(modelNumber))
            {
                throw new InvalidOperationException("Failed to generate a valid model number");
            }

            return string.Join("", modelNumber);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var arguments = ParseArguments(input);

            var rules = GenerateRules(arguments);

            var monad = new Monad(arguments);
            var generator = new ModelNumberGenerator(rules);

            var modelNumber = generator.GenerateSmallest(14);

            if (!monad.ValidateNumber(modelNumber))
            {
                throw new InvalidOperationException("Failed to generate a valid model number");
            }

            return string.Join("", modelNumber);
        }

        private static Dictionary<int, (int D, int Diff)> GenerateRules(List<(int A, int B, int C)> arguments)
        {
            var rules = new Dictionary<int, (int D, int Diff)>();

            var d = 0;
            var backlog = new Stack<(int D, int B, int C)>();
            backlog.Push((d, arguments.First().B, arguments.First().C));
            foreach ((var a, var b, var c) in arguments.Skip(1))
            {
                d++;
                var diff = backlog.Peek().C + b;
                if (diff < 9)
                {
                    //System.Console.WriteLine($"D{backlog.Peek().D} + {diff} == D{d}");
                    rules[backlog.Peek().D] = (d, diff);
                }

                if (a == 26)
                {
                    backlog.Pop();
                }
                else
                {
                    backlog.Push((d, b, c));
                }
            }

            return rules;
        }

        private static List<(int A, int B, int C)> ParseArguments(IEnumerable<string> input)
        {
            var lines = input.ToArray();

            var arguments = new List<(int A, int B, int C)>();
            for (int i = 0; i < lines.Length; i += 18)
            {
                var a = int.Parse(lines[i + 4].Split(" ").Last());
                var b = int.Parse(lines[i + 5].Split(" ").Last());
                var c = int.Parse(lines[i + 15].Split(" ").Last());

                arguments.Add((a, b, c));
            }

            //var d = 1;
            //foreach (var args in arguments)
            //{
            //    System.Console.WriteLine($"D{d}: a: {args.Item1}, b: {args.Item2}, c: {args.Item3}");
            //    d++;
            //}

            return arguments;
        }
    }
}
