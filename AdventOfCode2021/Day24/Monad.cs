﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day24
{
    class Monad
    {
        private readonly (int, int, int)[] arguments;

        private long w = 0;
        private long x = 0;
        private long y = 0;
        private long z = 0;

        public Monad(IEnumerable<(int, int, int)> arguments)
        {
            this.arguments = arguments.ToArray();
        }

        public bool ValidateNumber(int[] number)
        {
            w = x = y = z = 0;
            for(int i = 0; i < number.Length; i++)
            {
                FeedDigit(number[i], arguments[i]);
            }

            return z == 0;
        }

        /// inp w
        /// mul x 0
        /// add x z
        /// mod x 26
        /// div z A  <----
        /// add x B  <----
        /// eql x w
        /// eql x 0
        /// mul y 0
        /// add y 25
        /// mul y x
        /// add y 1
        /// mul z y
        /// mul y 0
        /// add y w
        /// add y C  <----
        /// mul y x
        /// add z y
        private void FeedDigit(int number, (int, int, int) arguments)
        {
            (var a, var b, var c) = arguments;

            var isMatch = ((z % 26) + b) == number;
            z /= a;
            if (!isMatch)
            {
                z *= 26;
                z += number + c;
            }

            //x = ((z % 26) + b) == number ? 0 : 1;
            //y = 25 * x + 1;
            //z /= a;
            //z *= y;
            //z += (number + c) * x;

            //w = number;
            //x = 0;
            //x += z;
            //x %= 26;
            //z /= a;
            //x += b;
            ////x = x == w ? 1 : 0;
            ////x = x == 0 ? 1 : 0;
            //x = x != w ? 1 : 0;
            //y = 0;
            //y += 25;
            //y *= x;
            //y += 1;
            //z *= y;
            //y = 0;
            //y += w;
            //y += c;
            //y *= x;
            //z += y;
        }
    }
}
