﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day24
{
    class ModelNumberGenerator
    {
        private readonly Dictionary<int, (int D, int diff)> rules;

        public ModelNumberGenerator(Dictionary<int, (int D, int diff)> rules)
        {
            this.rules = rules;
        }

        public int[] GenerateLargest(int numberOfDigits)
        {
            return GenerateNumber(numberOfDigits, (number, digitNumber) =>
            {
                (var d, var diff) = rules[digitNumber];
                if (diff >= 0)
                {
                    number[digitNumber] = 9 - diff;
                    number[d] = 9;
                }
                else
                {
                    number[digitNumber] = 9;
                    number[d] = 9 + diff;
                }
            });
        }

        public int[] GenerateSmallest(int numberOfDigits)
        {
            return GenerateNumber(numberOfDigits, (number, digitNumber) =>
            {
                (var d, var diff) = rules[digitNumber];
                if (diff >= 0)
                {
                    number[digitNumber] = 1;
                    number[d] = 1 + diff;
                }
                else
                {
                    number[digitNumber] = 1 - diff;
                    number[d] = 1;
                }
            });
        }

        private int[] GenerateNumber(int numberOfDigits, Action<int[], int> generateDigits)
        {
            var number = new int[numberOfDigits];

            for (int digitNumber = 0; digitNumber < numberOfDigits; digitNumber++)
            {
                if (!rules.ContainsKey(digitNumber))
                {
                    continue;
                }

                generateDigits(number, digitNumber);
            }

            return number;
        }
    }
}
