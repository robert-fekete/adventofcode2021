﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day7
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "328262";

        public string SecondExpected => "90040997";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "16,1,2,0,4,2,7,1,2,14"
                }, "37")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "16,1,2,0,4,2,7,1,2,14"
                }, "168")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var numbers = input.First().Split(",").Select(int.Parse);
            var crabs = new CrabFleet(numbers, i => i);
            var leastFuel = FindLeastVariance(crabs);

            return leastFuel.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var numbers = input.First().Split(",").Select(int.Parse);
            var crabs = new CrabFleet(numbers, i => (i + 1) * i / 2);
            var leastFuel = FindLeastVariance(crabs);

            return leastFuel.ToString();
        }

        private static long FindLeastVariance(CrabFleet crabs)
        {
            var depth = BinarySearch(1, 1000, i =>
            {
                var current = crabs.CalculateMoveCost(i);
                var next = crabs.CalculateMoveCost(i + 1);

                return current < next;
            });
            return crabs.CalculateMoveCost(depth);
        }

        private static int BinarySearch(int first, int last, Predicate<int> searchPredicate)
        {
            while (first < last)
            {
                var mid = first + (last - first) / 2;

                if (searchPredicate(mid))
                {
                    last = mid;
                }
                else
                {
                    first = mid + 1;
                }
            }

            return first;
        }
    }
}
