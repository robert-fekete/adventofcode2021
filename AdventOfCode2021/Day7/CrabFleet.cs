﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2021.Day7
{
    class CrabFleet
    {
        private readonly IEnumerable<int> crabs;
        private readonly Func<int, int> fuelCalculator;

        public CrabFleet(IEnumerable<int> crabs, Func<int, int> fuelCalculator)
        {
            this.crabs = crabs;
            this.fuelCalculator = fuelCalculator;
        }

        public int CalculateMoveCost(int targetDepth)
        {
            return crabs.Sum(n => fuelCalculator(Math.Abs(n - targetDepth)));
        }
    }
}
