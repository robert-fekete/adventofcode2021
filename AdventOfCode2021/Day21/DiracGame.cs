﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day21
{
    class DiracGame
    {
        private readonly int[] positions;
        private readonly Track track;
        private readonly IEnumerable<(int, int)> dice;
        private readonly int targetScore;

        private readonly Dictionary<(int, int, int, int, int), (long Wins1, long Wins2)> cache = new Dictionary<(int, int, int, int, int), (long, long)>();

        public DiracGame(IEnumerable<int> positions, Track track, IEnumerable<(int, int)> dice, int targetScore)
        {
            this.positions = positions.ToArray();
            this.track = track;
            this.dice = dice;
            this.targetScore = targetScore;
        }

        public (long, long) Play()
        {
            return Solve(1, 0, 0, positions[0], positions[1], 0);
        }

        private (long, long) Solve(long numberOfUniverses, int score1, int score2, int position1, int position2, int currentPlayer)
        {
            var key = (score1, score2, position1, position2, currentPlayer);
            if (cache.ContainsKey(key))
            {
                return (cache[key].Wins1 * numberOfUniverses, cache[key].Wins2 * numberOfUniverses);
            }

            var wins1 = 0L;
            var wins2 = 0L;
            if (score1 >= targetScore)
            {
                wins1 = numberOfUniverses;
            }
            else if (score2 >= targetScore)
            {
                wins2 = numberOfUniverses;
            }
            else
            {
                var nextPlayer = (currentPlayer + 1) % 2;
                foreach ((var steps, var occurrences) in dice)
                {
                    var currentPosition = currentPlayer == 0 ? position1 : position2;
                    var nextPosition = track.Step(currentPosition, steps);
                    var newNumberOfUniverses = numberOfUniverses * occurrences;

                    var newWins1 = 0L;
                    var newWins2 = 0L;
                    if (currentPlayer == 0)
                    {
                        (newWins1, newWins2) = Solve(newNumberOfUniverses, score1 + nextPosition, score2, nextPosition, position2, nextPlayer);
                    }
                    else
                    {
                        (newWins1, newWins2) = Solve(newNumberOfUniverses, score1, score2 + nextPosition, position1, nextPosition, nextPlayer);
                    }
                    wins1 += newWins1;
                    wins2 += newWins2;
                }
            }

            cache[key] = (wins1 / numberOfUniverses, wins2 / numberOfUniverses);
            return (wins1, wins2);
        }
    }
}
