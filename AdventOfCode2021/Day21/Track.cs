﻿namespace AdventOfCode2021.Day21
{
    class Track
    {
        private readonly int length = 10;
        
        public int Step(int position, int steps)
        {
            var value = position + steps;
            value = ((value - 1) % length) + 1;

            return value;
        }
    }
}
