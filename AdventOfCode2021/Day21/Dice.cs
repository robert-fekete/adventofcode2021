﻿namespace AdventOfCode2021.Day21
{
    class Dice
    {
        private int seed = 0;

        public int Rolls { get; private set; } = 0;

        public int Roll()
        {
            var number = seed + 1;
            seed++;
            seed %= 100;

            Rolls++;

            return number;
        }
    }
}
