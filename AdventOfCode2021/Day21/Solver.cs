﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day21
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "920580";

        public string SecondExpected => "647920021341197";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "Player 1 starting position: 4",
                    "Player 2 starting position: 8",
                }, "739785")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "Player 1 starting position: 4",
                    "Player 2 starting position: 8",
                }, "444356092776315")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var dice = new Dice();
            var track = new Track();
            var positions = ParsePositions(input);
            var game = new Game(positions, track, dice);

            game.PlayUntil(1000);

            var result = game.LoserScore * dice.Rolls;
            return result.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var track = new Track();
            var positions = ParsePositions(input).ToArray();
            var dice = new[] { (3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1) };

            var game = new DiracGame(positions, track, dice, 21);
            (var wins1, var wins2) = game.Play();

            var winner = wins1 > wins2 ? wins1 : wins2;
            return winner.ToString();
        }

        private static IReadOnlyCollection<int> ParsePositions(IEnumerable<string> input)
        {
            return input.Select(s => int.Parse(s.Split(" ").Last())).ToArray();
        }
    }
}
