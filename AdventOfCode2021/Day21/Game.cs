﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day21
{
    class Game
    {
        private readonly int[] positions;
        private readonly int[] scores;
        private readonly int numberOfPlayers;
        private readonly Track track;
        private readonly Dice dice;

        private int currentPlayer = 0;

        public Game(IEnumerable<int> positions, Track track, Dice dice)
        {
            numberOfPlayers = positions.Count();
            this.positions = positions.ToArray();
            scores = new int[numberOfPlayers];
            this.track = track;
            this.dice = dice;
        }

        public int LoserScore => scores.Min();
        public int Winner => Enumerable.Range(0, numberOfPlayers).OrderByDescending(i => scores[i]).First();

        public void PlayUntil(int winningScore)
        {
            while(scores.Max() < winningScore)
            {
                var steps = dice.Roll() + dice.Roll() + dice.Roll();
                positions[currentPlayer] = track.Step(positions[currentPlayer], steps);
                scores[currentPlayer] += positions[currentPlayer];

                currentPlayer = (currentPlayer + 1) % numberOfPlayers;
            }
        }
    }
}
