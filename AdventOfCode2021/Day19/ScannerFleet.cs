﻿using AdventOfCode2021.Day19.Math;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day19
{
    class ScannerFleet
    {
        private readonly Scanner[] scanners;
        private readonly int matchingBeaconsLimit;
        private readonly int referenceCoordinateSystem = 0;

        public ScannerFleet(IReadOnlyCollection<Scanner> scanners, int matchingBeaconsLimit)
        {
            this.scanners = scanners.ToArray();
            this.matchingBeaconsLimit = matchingBeaconsLimit;
        }

        public IReadOnlyCollection<Vector> NormalizeCoordinates(Func<Scanner, IEnumerable<Vector>> getCoordinates)
        {
            var transitions = FindTransitions();

            var bfs = new BFS();
            var parents = bfs.FindParents(transitions, referenceCoordinateSystem);

            var allCoordinates = new HashSet<Vector>();
            for (int i = 0; i < scanners.Length; i++)
            {
                var currentCoordinates = getCoordinates(scanners[i]);
                var currentCoordinateSystem = i;

                while (currentCoordinateSystem != referenceCoordinateSystem)
                {
                    var parent = parents[currentCoordinateSystem];
                    var transformation = transitions[currentCoordinateSystem][parent];

                    currentCoordinates = currentCoordinates.Select(b => transformation.Multiply(b));
                    currentCoordinateSystem = parent;
                }

                foreach (var beacon in currentCoordinates)
                {
                    allCoordinates.Add(beacon);
                }
            }

            return allCoordinates;
        }

        private Dictionary<int, Dictionary<int, Matrix>> FindTransitions()
        {
            var alignmentCollection = new Aligments();
            var alignments = alignmentCollection.GenerateAllAlignments();

            var transitions = new Dictionary<int, Dictionary<int, Matrix>>();
            for (int i = 0; i < scanners.Length; i++)
            {
                transitions[i] = new Dictionary<int, Matrix>();
            }

            var remainders = new List<int>(Enumerable.Range(0, scanners.Length).Where(i => i != referenceCoordinateSystem));
            var backlog = new Queue<int>();

            backlog.Enqueue(referenceCoordinateSystem);

            while (remainders.Count > 0)
            {
                if (!backlog.Any())
                {
                    throw new InvalidOperationException("The chain is broken");
                }

                var current = backlog.Dequeue();

                foreach (var next in remainders.ToArray())
                {
                    var matchingAlignment = scanners[current].GetMatchingAlignment(scanners[next], alignments, matchingBeaconsLimit);
                    if (matchingAlignment.Any())
                    {
                        transitions[current][next] = matchingAlignment.Single().SourceToTarget;
                        transitions[next][current] = matchingAlignment.Single().TargetToSource;

                        remainders.Remove(next);
                        backlog.Enqueue(next);

                        //Console.WriteLine($"Scanner {current} sees Scanner {next}");
                    }
                }
            }

            return transitions;
        }
    }
}
