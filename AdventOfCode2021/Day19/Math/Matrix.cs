﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day19.Math
{
    class Matrix
    {
        private readonly long[][] rows;

        public Matrix(long[][] rows)
        {
            this.rows = rows;
        }

        public Matrix(Matrix other)
        {
            rows = new long[other.rows.Length][];

            for (int y = 0; y < other.rows.Length; y++)
            {
                rows[y] = new long[other.rows[y].Length];
                for (int x = 0; x < other.rows[y].Length; x++)
                {
                    rows[y][x] = other.rows[y][x];
                }
            }
        }

        public Matrix Multiply(Matrix other)
        {
            var newRows = new List<long[]>();
            for (int ri = 0; ri < rows.Length; ri++)
            {
                var row = new List<long>();
                for (int ci = 0; ci < rows[ri].Length; ci++)
                {
                    var value = 0L;
                    for (int iterator = 0; iterator < rows[ri].Length; iterator++)
                    {
                        value += rows[ri][iterator] * other.rows[iterator][ci];
                    }
                    row.Add(value);
                }
                newRows.Add(row.ToArray());
            }

            return new Matrix(newRows.ToArray());
        }

        public Vector Multiply(Vector other)
        {
            var row = new List<long>();
            for (int ri = 0; ri < rows.Length; ri++)
            {
                var value = 0L;
                for (int iterator = 0; iterator < rows.Length; iterator++)
                {
                    value += rows[ri][iterator] * other[iterator];
                }
                row.Add(value);
            }

            return new Vector(row);
        }

        public Matrix Multiply(long scalar)
        {
            var newRows = new List<long[]>();
            foreach (var r in rows)
            {
                var row = new List<long>();
                foreach (var c in r)
                {
                    row.Add(c * scalar);
                }
                newRows.Add(row.ToArray());
            }

            return new Matrix(newRows.ToArray());
        }

        public Matrix Transpose()
        {
            var newRows = new List<long[]>();
            for (int ri = 0; ri < rows.Length; ri++)
            {
                var row = new List<long>();
                for (int ci = 0; ci < rows[ri].Length; ci++)
                {
                    row.Add(rows[ci][ri]);
                }
                newRows.Add(row.ToArray());
            }

            return new Matrix(newRows.ToArray());
        }

        public Matrix Grow()
        {
            var newRows = new List<long[]>();
            for (int ri = 0; ri < rows.Length; ri++)
            {
                var row = new List<long>();
                for (int ci = 0; ci < rows[ri].Length; ci++)
                {
                    row.Add(rows[ri][ci]);
                }
                row.Add(0);
                newRows.Add(row.ToArray());
            }

            {
                var row = new List<long>();
                for (int ci = 0; ci < rows[0].Length; ci++)
                {
                    row.Add(0);
                }
                row.Add(1);
                newRows.Add(row.ToArray());
            }

            return new Matrix(newRows.ToArray());
        }

        public override string ToString()
        {
            return string.Join("\n", rows.Select(r => $"[{string.Join(",", r)}]"));
        }
    }
}
