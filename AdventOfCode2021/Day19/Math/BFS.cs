﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day19.Math
{
    class BFS
    {
        private const int ROOT = -1;

        public Dictionary<int, int> FindParents(Dictionary<int, Dictionary<int, Matrix>> transitions, int root)
        {
            var backlog = new Queue<(int, int)>();

            var visited = new HashSet<int>();

            backlog.Enqueue((root, ROOT));
            var parents = new Dictionary<int, int>();
            while (backlog.Any())
            {
                (var current, var parent) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                parents[current] = parent;

                foreach (var next in transitions[current].Keys)
                {
                    backlog.Enqueue((next, current));
                }
            }

            return parents;
        }
    }
}
