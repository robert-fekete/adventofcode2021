﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day19.Math
{
    class Vector
    {
        private readonly int hash;
        private readonly long[] values;

        public Vector(IEnumerable<long> values)
        {
            this.values = values.ToArray();
            hash = GetHashCodeExpensive();
        }

        public long Length
        {
            get
            {
                var sum = 0L;
                foreach(var value in values)
                {
                    sum += System.Math.Abs(value);
                }

                return sum;
            }
        }

        public long this[int index] => values[index];

        public Vector Subtract(Vector other)
        {
            var newValues = new long[values.Length];
            for(int i = 0; i < values.Length; i++)
            {
                newValues[i] = values[i] - other.values[i];
            }

            return new Vector(newValues);
        }

        public Vector Add(Vector other)
        {
            var newValues = new long[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                newValues[i] = values[i] + other.values[i];
            }

            return new Vector(newValues);
        }

        public Matrix ToTranslationMatrix()
        {
            var rows = new long[values.Length + 1][];
            for (int i = 0; i < values.Length; i++)
            {
                rows[i] = new long[values.Length + 1];
                for (int j = 0; j < values.Length; j++)
                {
                    if (i == j)
                    {
                        rows[i][j] = 1;
                    }
                    else
                    {
                        rows[i][j] = 0;
                    }
                }
                rows[i][values.Length] = values[i];
            }

            rows[values.Length] = new long[values.Length + 1];
            for (int i = 0; i < values.Length; i++)
            {
                rows[values.Length][i] = 0;
            }
            rows[values.Length][values.Length] = 1;

            return new Matrix(rows);
        }

        public Vector Grow()
        {
            var newValues = new long[values.Length + 1];
            Array.Copy(values, newValues, values.Length);
            newValues[values.Length] = 1;

            return new Vector(newValues);
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public int GetHashCodeExpensive()
        {
            var prime = 479;
            var acc = prime;
            foreach (var value in values)
            {
                acc = acc * prime + (int)value;
            }
            return acc;
        }

        public override bool Equals(object? obj)
        {
            return obj is Vector other && Equals(other);
        }

        public bool Equals(Vector other)
        {
            return hash == other.hash;
        }

        public override string ToString()
        {
            return $"({string.Join(",", values)})";
        }
    }
}
