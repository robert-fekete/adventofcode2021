﻿using AdventOfCode2021.Day19.Math;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day19
{
    class Scanner
    {
        private readonly IEnumerable<Vector> beacons;
        private readonly IReadOnlyCollection<Sample> samples;

        public Scanner(string name, IEnumerable<Vector> beacons)
        {
            Name = name;
            this.beacons = beacons;
            samples = GetAllSamples();
        }

        public string Name { get; }

        public IEnumerable<Vector> GetTransformableBeacons()
        {
            return beacons.Select(b => b.Grow());
        }

        public IEnumerable<(Matrix SourceToTarget, Matrix TargetToSource)> GetMatchingAlignment(Scanner other, IReadOnlyCollection<Matrix> alignments, int matchingBeaconLimit)
        {
            var matchingAlignment = new List<(Matrix, Matrix)>();

            var matchingDistancesLimit = matchingBeaconLimit * (matchingBeaconLimit - 1);
            var matchingDistances = QuickMatch(other);
            if (matchingDistances.Values.Sum() < matchingDistancesLimit)
            {
                return matchingAlignment;
            }

            var otherSamples = other.samples;

            foreach(var alignment in alignments)
            {
                Vector? sourceReferencePoint = null;
                Vector? targetReferencePoint = null;

                var matchingBeacons = new HashSet<Vector>();

                foreach(var sample in samples)
                {
                    if (!matchingDistances.ContainsKey(sample.Length))
                    {
                        continue;
                    }

                    var rotatedSample = sample.Rotate(alignment);

                    foreach (var otherSample in otherSamples)
                    {
                        if (rotatedSample.Equals(otherSample.DifferenceVector))
                        {
                            sourceReferencePoint = sample.Start;
                            targetReferencePoint = otherSample.Start;
                            matchingBeacons.Add(sample.Start);
                            matchingBeacons.Add(sample.End);
                            break;
                        }
                    }
                }

                if (matchingBeacons.Count >= matchingBeaconLimit)
                {
                    if (sourceReferencePoint is null || targetReferencePoint is null)
                    {
                        throw new InvalidOperationException("Not possible");
                    }

                    var rotatedSource = alignment.Multiply(sourceReferencePoint);
                    var sourceToTargetTranslation = targetReferencePoint.Subtract(rotatedSource).ToTranslationMatrix();
                    var sourceToTargetRotation = alignment.Grow();
                    var sourceToTargetTransformation = sourceToTargetTranslation.Multiply(sourceToTargetRotation);

                    var targetToSourceTranslation = rotatedSource.Subtract(targetReferencePoint).ToTranslationMatrix();
                    var targetToSourceRotation = alignment.Transpose().Grow();
                    var targetToSourceTransformation = targetToSourceRotation.Multiply(targetToSourceTranslation);

                    matchingAlignment.Add((sourceToTargetTransformation, targetToSourceTransformation));
                    
                    return matchingAlignment;
                }
            }

            return matchingAlignment;
        }

        private Dictionary<long, int> QuickMatch(Scanner other)
        {
            var ownLengths = CountLengths();
            var otherLengths = other.CountLengths();

            var matchingLengths = new Dictionary<long, int>();
            foreach(var length in ownLengths.Keys)
            {
                if (!otherLengths.ContainsKey(length))
                {
                    continue;
                }

                matchingLengths[length] = System.Math.Min(ownLengths[length], otherLengths[length]);
            }

            return matchingLengths;
        }

        private Dictionary<long, int> CountLengths()
        {
            var lengths = new Dictionary<long, int>();

            foreach(var sample in samples)
            {
                var length = sample.Length;
                if (!lengths.ContainsKey(length))
                {
                    lengths[length] = 0;
                }
                lengths[length]++;
            }

            return lengths;
        }

        private IReadOnlyCollection<Sample> GetAllSamples()
        {
            var samples = new List<Sample>();
            foreach(var a in beacons)
            {
                foreach(var b in beacons)
                {
                    if (a.Equals(b))
                    {
                        continue;
                    }

                    samples.Add(new Sample(a, b));
                }
            }

            return samples;
        }
    }
}
