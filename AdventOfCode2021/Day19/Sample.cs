﻿using AdventOfCode2021.Day19.Math;

namespace AdventOfCode2021.Day19
{
    class Sample
    {
        public Sample(Vector start, Vector end)
        {
            Start = start;
            End = end;
            DifferenceVector = start.Subtract(end);
        }

        public Vector Start { get; }
        public Vector End { get; }
        public Vector DifferenceVector { get; }

        public long Length => DifferenceVector.Length;

        public Vector Rotate(Matrix alignment)
        {
            return alignment.Multiply(DifferenceVector);
        }
    }
}
