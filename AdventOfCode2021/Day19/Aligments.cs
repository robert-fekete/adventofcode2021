﻿using AdventOfCode2021.Day19.Math;
using System.Collections.Generic;

namespace AdventOfCode2021.Day19
{
    class Aligments
    {
        private readonly Matrix Mx;
        private readonly Matrix My;
        private readonly Matrix Myt;
        private readonly Matrix Mz;
        private readonly Matrix Mzt;

        public Aligments()
        {
            Mx = new Matrix(new[]
            {
                new []{1L, 0, 0},
                new []{0L, 0, -1},
                new []{0L, 1, 0},
            });

            My = new Matrix(new[]
            {
                new []{0L, 0, 1},
                new []{0L, 1, 0},
                new []{-1L, 0, 0},
            });
            Myt = My.Transpose();

            Mz = new Matrix(new[]
            {
                new []{0L, -1, 0},
                new []{1L, 0, 0},
                new []{0L, 0, 1},
            });
            Mzt = Mz.Transpose();
        }

        public IReadOnlyCollection<Matrix> GenerateAllAlignments()
        {
            var alignments = new List<Matrix>();
            var iterator = new Matrix(Mx);
            var reverser = Mz.Multiply(Mz);

            for (int i = 0; i < 4; i++)
            {
                alignments.Add(iterator);
                alignments.Add(iterator.Multiply(My));
                alignments.Add(iterator.Multiply(Myt));
                alignments.Add(iterator.Multiply(Mz));
                alignments.Add(iterator.Multiply(Mzt));
                alignments.Add(iterator.Multiply(reverser));

                iterator = iterator.Multiply(Mx);
            }

            return alignments;
        }
    }
}
