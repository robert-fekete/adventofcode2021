﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day22
{
    class Cube
    {
        private readonly (int Min, int Max) rangeX;
        private readonly (int Min, int Max) rangeY;
        private readonly (int Min, int Max) rangeZ;

        public Cube((int, int) rangeX, (int, int) rangeY, (int, int) rangeZ)
        {
            this.rangeX = rangeX;
            this.rangeY = rangeY;
            this.rangeZ = rangeZ;
        }

        public long Volume => (rangeX.Max - rangeX.Min + 1L) * (rangeY.Max - rangeY.Min + 1L) * (rangeZ.Max - rangeZ.Min + 1L);
        private bool IsInvalid => rangeX.Min > rangeX.Max || rangeY.Min > rangeY.Max || rangeZ.Min > rangeZ.Max;

        public bool IsSmall(int limit)
        {
            return -limit <= rangeX.Min && rangeX.Max <= limit &&
                -limit <= rangeY.Min && rangeY.Max <= limit &&
                -limit <= rangeZ.Min && rangeZ.Max <= limit;
        }

        public bool HasIntersection(Cube other)
        {
            return HasIntersection(rangeX, other.rangeX) && HasIntersection(rangeY, other.rangeY) && HasIntersection(rangeZ, other.rangeZ);
        }

        private bool HasIntersection((int Min, int Max) range1, (int Min, int Max) range2)
        {
            return range1.Min <= range2.Max && range1.Max >= range2.Min;
        }
        
        public IEnumerable<Cube> Subtract(Cube other)
        {
            var segmentsX = GetSegments(rangeX, other.rangeX);
            var segmentsY = GetSegments(rangeY, other.rangeY);
            var segmentsZ = GetSegments(rangeZ, other.rangeZ);

            var sectors = new List<Cube>();
            foreach((var isIntersectX, var minX, var maxX) in segmentsX)
            {
                foreach ((var isIntersectY, var minY, var maxY) in segmentsY)
                {
                    foreach ((var isIntersectZ, var minZ, var maxZ) in segmentsZ)
                    {
                        if (isIntersectX && isIntersectY && isIntersectZ)
                        {
                            continue;
                        }

                        var segment = new Cube((minX, maxX), (minY, maxY), (minZ, maxZ));
                        if (!segment.IsInvalid)
                        {
                            sectors.Add(segment);
                        }
                    }
                }
            }

            return sectors;
        }

        private (bool, int, int)[] GetSegments((int Min, int Max) range1, (int Min, int Max) range2)
        {
            var a = (Math.Min(range1.Min, range2.Min), Math.Max(range1.Min, range2.Min) - 1);
            var b = (Math.Max(range1.Min, range2.Min), Math.Min(range1.Max, range2.Max));
            var c = (Math.Min(range1.Max, range2.Max) + 1, Math.Max(range1.Max, range2.Max));

            var segments = new List<(bool, int, int)>();
            if (a.Item1 <= a.Item2 && range1.Min <= a.Item1)
            {
                segments.Add((false, a.Item1, a.Item2));
            }
            if (b.Item1 <= b.Item2)
            {
                segments.Add((true, b.Item1, b.Item2));
            }
            if (c.Item1 <= c.Item2 && range1.Max >= c.Item2)
            {
                segments.Add((false, c.Item1, c.Item2));
            }

            return segments.ToArray();
        }

        public override string ToString()
        {
            return $"x={rangeX.Min}..{rangeX.Max},y={rangeY.Min}..{rangeY.Max},z={rangeZ.Min}..{rangeZ.Max}";
        }
    }
}
