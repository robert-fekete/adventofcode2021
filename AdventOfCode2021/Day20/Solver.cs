﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day20
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "5884";

        public string SecondExpected => "19043";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#",
                    "",
                    "#..#.",
                    "#....",
                    "##..#",
                    "..#..",
                    "..###",
                }, "35")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#",
                    "",
                    "#..#.",
                    "#....",
                    "##..#",
                    "..#..",
                    "..###",
                }, "3351")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var image = Solve(input, 2);

            return image.LitPixelCount.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var image = Solve(input, 50);

            return image.LitPixelCount.ToString();
        }

        private static Image Solve(IEnumerable<string> input, int iterations)
        {
            var code = input.First();
            var rawImage = input.Skip(2).Select(s => s.ToArray()).ToArray();

            var image = new Image(rawImage);
            for(int i = 0; i < iterations; i++)
            {
                image = image.Enhance(code);
            }

            return image;
        }
    }
}
