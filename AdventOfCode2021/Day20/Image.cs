﻿using System;
using System.Linq;

namespace AdventOfCode2021.Day20
{
    class Image
    {
        private char[][] image;
        private int infiniteValue;

        public Image(char[][] image)
            : this(image, 0)
        { }

        private Image(char[][] image, int infiniteValue)
        {
            this.image = image;
            this.infiniteValue = infiniteValue;
        }

        public int LitPixelCount => image.Sum(l => l.Count(c => c == '#'));
        private int Width => image[0].Length;
        private int Height => image.Length;

        public Image Enhance(string code)
        {
            var newImage = new char[Height + 2][];
            for (int y = 0; y < Height + 2; y++) 
            {
                newImage[y] = new char[Width + 2];
            }

            for(int x = 0; x < Width + 2; x++)
            {
                var iterator = 0;
                iterator = iterator * 8 + GetBinaryValue(x - 2, -2);
                iterator = iterator * 8 + GetBinaryValue(x - 2, -1);
                iterator = iterator * 8 + GetBinaryValue(x - 2, 0);

                newImage[0][x] = code[iterator];

                for(int y = 0; y < Height + 1; y++)
                {
                    iterator = (iterator * 8 + GetBinaryValue(x - 2, y + 1)) % 512;
                    newImage[y + 1][x] = code[iterator];
                }
            }

            var newInfiniteValue = infiniteValue;
            if (code[0] == '#' && code[511] == '.')
            {
                newInfiniteValue = (newInfiniteValue + 1) % 2;
            }

            return new Image(newImage, newInfiniteValue);
        }

        private int GetBinaryValue(int x, int y)
        {
            return 4 * GetPixel(x, y) + 2 * GetPixel(x + 1, y) + GetPixel(x + 2, y);
        }

        private int GetPixel(int x, int y)
        {
            if (!InBounds(x, 0, Width - 1) || !InBounds(y, 0, Height - 1))
            {
                return infiniteValue;
            }

            if (image[y][x] == '#')
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private static bool InBounds(int value, int min, int max)
        {
            return min <= value && value <= max;
        }

        public void Print()
        {
            for (int y = -2; y < image.Length + 2; y++)
            {
                for(int x = -2; x < image[0].Length + 2; x++)
                {
                    Console.Write(GetPixel(x, y) == 1 ? '#' : '.');
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
