﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day25
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "414";

        public string SecondExpected => "Finished";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "v...>>.vv>",
                    ".vv>>.vv..",
                    ">>.>v>...v",
                    ">>v>>.>.v.",
                    "v>v.vv.v..",
                    ">.>>..v...",
                    ".vv..>.>v.",
                    "v.v..>>v.v",
                    "....v..v.>",
                }, "58")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var herd = new SeaCucumberHerd(input);

            var count = 0L;
            while (!herd.IsStationary)
            {
                herd.Move();
                count++;
            }

            return count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return "Finished";
        }
    }
}
