﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day25
{
    class SeaCucumberHerd
    {
        private char[][] seaCucumbers;

        public SeaCucumberHerd(IEnumerable<string> seaCucumbers)
        {
            this.seaCucumbers = seaCucumbers.Select(line => line.Select(c => c == '.' ? default : c).ToArray()).ToArray();
        }

        public bool IsStationary { get; private set; }

        public void Move()
        {
            var nextState = GenerateEmptyState();
            var didChange = false;

            didChange |= MoveEastFacing(nextState);
            didChange |= MoveSouthFacing(nextState);

            seaCucumbers = nextState;

            IsStationary = !didChange;
        }

        private bool MoveEastFacing(char[][] nextState)
        {
            return Move('>', GetNextEastPosition, nextState, seaCucumbers, seaCucumbers);
        }

        private bool MoveSouthFacing(char[][] nextState)
        {
            return Move('v', GetNextSouthPosition, nextState, nextState, seaCucumbers);
        }

        private bool Move(char type, Func<int, int, (int, int)> getNextPosition, char[][] nextState, char[][] referenceEast, char[][] referenceSouth)
        {
            var didChange = false;
            for (int y = 0; y < seaCucumbers.Length; y++)
            {
                for (int x = 0; x < seaCucumbers[0].Length; x++)
                {
                    if (seaCucumbers[y][x] == type)
                    {
                        (var nextX, var nextY) = getNextPosition(x, y);
                        if (referenceEast[nextY][nextX] != '>' && referenceSouth[nextY][nextX] != 'v')
                        {
                            nextState[nextY][nextX] = type;
                            didChange = true;
                        }
                        else
                        {
                            nextState[y][x] = type;
                        }
                    }
                }
            }

            return didChange;
        }

        public void Print()
        {
            Print(seaCucumbers);
        }

        private (int, int) GetNextEastPosition(int x, int y)
        {
            var nextX = x + 1;
            if (nextX >= seaCucumbers[0].Length)
            {
                nextX = 0;
            }

            return (nextX, y);
        }

        private (int, int) GetNextSouthPosition(int x, int y)
        {
            var nextY = y + 1;
            if (nextY >= seaCucumbers.Length)
            {
                nextY = 0;
            }

            return (x, nextY);
        }

        private char[][] GenerateEmptyState()
        {
            var state = new char[seaCucumbers.Length][];
            for (int i = 0; i < seaCucumbers.Length; i++)
            {
                state[i] = new char[seaCucumbers[i].Length];
            }

            return state;
        }

        private void Print(char[][] heard)
        {
            for (int y = 0; y < heard.Length; y++)
            {
                for (int x = 0; x < heard[0].Length; x++)
                {
                    Console.Write(heard[y][x] == default ? '.' : heard[y][x]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
