﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day6
{
    internal class School
    {
        private IEnumerable<int> currentGeneration;

        public School(IEnumerable<int> initialGeneration)
        {
            currentGeneration = initialGeneration;
        }

        public long Count => currentGeneration.LongCount();

        internal void Age()
        {
            var newGeneration = new List<int>();
            var newFish = 0;
            foreach(var fish in currentGeneration)
            {
                var newAge = fish - 1;
                if (newAge < 0)
                {
                    newAge = 6;
                    newFish++;
                }
                newGeneration.Add(newAge);
            }

            newGeneration.AddRange(Enumerable.Repeat(8, newFish));

            currentGeneration = newGeneration;
        }

        public void Print()
        {
            Console.WriteLine(string.Join(", ", currentGeneration));
        }
    }
}