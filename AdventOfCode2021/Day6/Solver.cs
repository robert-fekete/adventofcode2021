﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day6
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "376194";

        public string SecondExpected => "1693022481538";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "3,4,3,1,2"
                }, "5934")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "3,4,3,1,2"
                }, "26984457539")
                .AddTestCase(new[]
                {
                   "3,4,3,1,2"
                }, "26984457539")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var fish = input.First().Split(",").Select(int.Parse);
            var schoolOfFish = new School(fish);

            return SimulateGenerations(schoolOfFish, 80);
        }

        private static string SimulateGenerations(School schoolOfFish, int numberOfGenerations)
        {
            for (int i = 0; i < numberOfGenerations; i++)
            {
                schoolOfFish.Age();
                //schoolOfFish.Print();
            }

            return schoolOfFish.Count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var fish = input.First().Split(",").Select(int.Parse);

            var total = SolveWithDp(fish, 256);

            return total.ToString();
        }

        private long SolveWithDp(IEnumerable<int> fish, int generations)
        {
            var dp = new long[generations + 1];  // The number of offsprings a fish will have if it's 0 days old and there is 'i' days left
                                                 

            dp[0] = 0; // There is 0 day left, so it won't have a chance to make an offspring

            for (int i = 1; i < 8; i++)
            {
                dp[i] = 1; // There is enough time to make 1 offspring, but not 2
            }

            dp[8] = 2; // There is enough time to make 2 offsprings, but none of the offsprings will reach maturity in time

            for (int i = 9; i <= generations; i++)
            {
                dp[i] = dp[i - 7] + // The number of offsprings the fish had when it was 0 days old the last time
                        1 +         // Plus, the offspring it will make the next day
                        dp[i - 9];  // Plus, the number of sub-offsprings that offspring will make when it reached maturity
            }

            var total = fish.LongCount(); // The initial number of fish
            foreach(var age in fish)
            {
                total += dp[generations - age]; // Make the initial fish 0 days old and read the value
            }

            return total;
        }

        private long SolveWithCyclic(IEnumerable<int> fish, int generations)
        {
            var current = new long[9];
            var next = new long[9];

            foreach (var age in fish)
            {
                current[age]++;
            }

            for (int gen = 0; gen < generations; gen++)
            {
                for (int i = 0; i < 8; i++)
                {
                    next[i] = current[i + 1];
                }
                next[6] += current[0];
                next[8] = current[0];

                var temp = current;
                current = next;
                next = temp;
            }

            return current.Sum();
        }
    }
}
