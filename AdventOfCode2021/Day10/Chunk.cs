﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day10
{
    class Chunk
    {
        private readonly IEnumerable<ChunkItem> remaining;
        private readonly IEnumerable<ChunkItem> preprocessed;

        public Chunk(IEnumerable<ChunkItem> items)
        {
            (remaining, preprocessed) = PreprocessChunk(items);
        }

        public bool IsCorrupted => remaining.Any();

        public int GetCorruptionScore()
        {
            if (!IsCorrupted)
            {
                throw new InvalidOperationException("Chunk is not corrupted");
            }
            var corruptedCharacer = remaining.First();
            return corruptedCharacer.CorruptionScore;
        }

        public long GetIncompleteScore()
        {
            var score = 0L;
            foreach (var item in preprocessed)
            {
                score *= 5;
                score += item.IncompleteScore;
            }

            return score;
        }

        private (IEnumerable<ChunkItem>, IEnumerable<ChunkItem>) PreprocessChunk(IEnumerable<ChunkItem> items)
        {
            var remaining = new Stack<ChunkItem>(items.Reverse());
            var history = new Stack<ChunkItem>();
            while (remaining.Any())
            {
                var item = remaining.Pop();
                if (item.IsOpening)
                {
                    history.Push(item);
                }
                else if (item.IsClosing)
                {
                    var top = history.Pop();
                    if (!top.IsMatching(item))
                    {
                        remaining.Push(item);
                        break;
                    }
                }
                else
                {
                    throw new InvalidOperationException("Invalid character in chunk");
                }
            }

            return (remaining, history);
        }

        public override string ToString()
        {
            return $"{string.Join("", preprocessed)},{string.Join("", remaining)}";
        }
    }
}
