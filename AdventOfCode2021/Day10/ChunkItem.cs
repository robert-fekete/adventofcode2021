﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day10
{
    class ChunkItem
    {
        private readonly Dictionary<char, int> corruptionScore = new Dictionary<char, int>
        {
            { ')', 3 },
            { ']', 57 },
            { '}', 1197 },
            { '>', 25137 },
        };
        private readonly Dictionary<char, int> incompleteScore = new Dictionary<char, int>
        {
            { '(', 1 },
            { '[', 2 },
            { '{', 3 },
            { '<', 4 },
        };

        private readonly Dictionary<char, char> pairs = new Dictionary<char, char>
        {
            { '<', '>' },
            { '(', ')' },
            { '[', ']' },
            { '{', '}' },
        };

        private readonly char value;

        public ChunkItem(char value)
        {
            this.value = value;
        }

        public bool IsOpening => pairs.Keys.Contains(value);
        public bool IsClosing => pairs.Values.Contains(value);
        public bool IsMatching(ChunkItem item) => pairs[value] == item.value;
        public int CorruptionScore => corruptionScore[value];
        public int IncompleteScore => incompleteScore[value];

        public override string ToString()
        {
            return value.ToString();
        }

    }
}
