﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day10
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "265527";

        public string SecondExpected => "3969823589";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "[({(<(())[]>[[{[]{<()<>>",
                    "[(()[<>])]({[<{<<[]>>(",
                    "{([(<{}[<>[]}>{[]{[(<()>",
                    "(((({<>}<{<{<>}{[]{[]{}",
                    "[[<[([]))<([[{}[[()]]]",
                    "[{[{({}]{}}([{[{{{}}([]",
                    "{<[[]]>}<{[{[{[]{()[[[]",
                    "[<(<(<(<{}))><([]([]()",
                    "<{([([[(<>()){}]>(<<{{",
                    "<{([{{}}[<[[[<>{}]]]>[]]",
                }, "26397")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "[({(<(())[]>[[{[]{<()<>>",
                    "[(()[<>])]({[<{<<[]>>(",
                    "{([(<{}[<>[]}>{[]{[(<()>",
                    "(((({<>}<{<{<>}{[]{[]{}",
                    "[[<[([]))<([[{}[[()]]]",
                    "[{[{({}]{}}([{[{{{}}([]",
                    "{<[[]]>}<{[{[{[]{()[[[]",
                    "[<(<(<(<{}))><([]([]()",
                    "<{([([[(<>()){}]>(<<{{",
                    "<{([{{}}[<[[[<>{}]]]>[]]",
                }, "288957")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var chuncks = ParseChuncks(input);
            var score = CollectChunks(chuncks, c => c.IsCorrupted).Select(c => c.GetCorruptionScore()).Sum();

            return score.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var chuncks = ParseChuncks(input);
            var incompleteScores = CollectChunks(chuncks, c => !c.IsCorrupted).Select(c => c.GetIncompleteScore());

            long median = FindMedian(incompleteScores);
            return median.ToString();
        }

        private static IEnumerable<Chunk> CollectChunks(IEnumerable<Chunk> chuncks, Predicate<Chunk> predicate)
        {
            var collectedChunks = new List<Chunk>();
            foreach (var chunk in chuncks)
            {
                if (predicate(chunk))
                {
                    collectedChunks.Add(chunk);
                }
            }

            return collectedChunks;
        }

        private static long FindMedian(IEnumerable<long> numbers)
        {
            var median = numbers.OrderBy(s => s)
                                .Skip((numbers.Count() - 1) / 2)
                                .First();
            return median;
        }

        private IEnumerable<Chunk> ParseChuncks(IEnumerable<string> input)
        {
            return input.Select(l => new Chunk(l.Select(c => new ChunkItem(c))));
        }
    }
}
