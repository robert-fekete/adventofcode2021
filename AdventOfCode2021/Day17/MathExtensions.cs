﻿using System;

namespace AdventOfCode2021.Day17
{
    class MathExtensions
    {
        public static (double X1, double X2) SolveQuadratic(double a, double b, double c)
        {
            var D = Math.Sqrt(b * b - 4 * a * c);
            var x1 = (-b + D) / (2 * a);
            var x2 = (-b - D) / (2 * a);

            return (x1, x2);
        }
    }
}
