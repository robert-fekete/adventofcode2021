﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day17
{
    class TrajectoryCalculator
    {
        private readonly Probe probe;
        private readonly Target target;

        public TrajectoryCalculator(Probe probe, Target target)
        {
            this.probe = probe;
            this.target = target;
        }

        public int CalculateNumberOfTrajectories((int X, int Y) min, (int X, int Y) max)
        {
            var maxSteps = 2 * (Math.Abs(max.Y) + 1) + 1;
            var points = new HashSet<(int, int)>();
            var lastSize = 0;
            for (int numberOfSteps = 1; numberOfSteps <= maxSteps; numberOfSteps++)
            {
                var degradation = (numberOfSteps - 1) * numberOfSteps / 2.0;
                
                var minVelocityX = (int)Math.Ceiling((target.MinX + degradation) / numberOfSteps);
                var maxVelocityX = (int)Math.Floor((target.MaxX + degradation) / numberOfSteps);

                if (minVelocityX <= numberOfSteps)
                {
                    continue;
                }

                // Shooting downwards
                var maxDownVelocityY = (int)-Math.Ceiling((-target.MaxY - degradation) / numberOfSteps);
                var minDownVelocityY = (int)-Math.Floor((-target.MinY - degradation) / numberOfSteps);

                // Shooting upwards
                // ??? 


                for(int y = minDownVelocityY; y <= maxDownVelocityY; y++)
                {
                    for(int x = minVelocityX; x <= maxVelocityX; x++)
                    {
                        points.Add((x, y));
                    }
                }

                if (lastSize == points.Count)
                {
                    break;
                }

                lastSize = points.Count;
            }

            return points.Count;
        }

        public int SimulateAllTrajectory((int X, int Y) min, (int X, int Y) max)
        {
            int counter = 0;
            for (int x = min.X; x <= max.X; x++)
            {
                for (int y = min.Y; y <= max.Y; y++)
                {
                    if (probe.IsHit(target, (x, y)))
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }
    }
}
