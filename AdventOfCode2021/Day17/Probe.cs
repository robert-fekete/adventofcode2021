﻿using System.Collections.Generic;

namespace AdventOfCode2021.Day17
{
    class Probe
    {
        private readonly (int, int) startingPosition;

        public Probe((int, int) startingPosition)
        {
            this.startingPosition = startingPosition;
        }

        public IReadOnlyCollection<(int X, int Y)> SimulateTilTarget(Target target, (int, int) initialVelocity)
        {
            var points = new List<(int, int)>() { startingPosition };

            (int X, int Y) velocity = initialVelocity;
            (int X, int Y) position = startingPosition;
            while(!IsInTarget(position, target))
            {
                position = CalculateNextPosition(velocity, position);
                velocity = CalculateNextVelocity(velocity);

                points.Add(position);
            }

            return points;
        }

        public bool IsHit(Target target, (int, int) initialVelocity)
        {
            (int X, int Y) velocity = initialVelocity;
            (int X, int Y) position = startingPosition;
            while (position.X <= target.MaxX && position.Y >= target.MinY)
            {
                position = CalculateNextPosition(velocity, position);
                velocity = CalculateNextVelocity(velocity);

                if (IsInTarget(position, target))
                {
                    return true;
                }
            }

            return false;
        }

        private static (int, int) CalculateNextPosition((int X, int Y) velocity, (int X, int Y) position)
        {
            return (position.X + velocity.X, position.Y + velocity.Y);
        }

        private static (int X, int Y) CalculateNextVelocity((int X, int Y) velocity)
        {
            velocity = (ReduceInAbsoluteValue(velocity.X), velocity.Y - 1);
            return velocity;
        }

        private static bool IsInTarget((int X, int Y) position, Target target)
        {
            return target.MinX <= position.X && position.X <= target.MaxX
                && target.MinY <= position.Y && position.Y <= target.MaxY;
        }

        private static int ReduceInAbsoluteValue(int x)
        {
            if (x == 0)
            {
                return 0;
            }
            else if(x > 0)
            {
                return x - 1;
            }
            else
            {
                return x + 1;
            }
        }
    }
}
