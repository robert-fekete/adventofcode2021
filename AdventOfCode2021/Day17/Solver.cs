﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day17
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "7503";

        public string SecondExpected => "3229";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "target area: x=20..30, y=-10..-5"
                }, "45")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                   "target area: x=20..30, y=-10..-5"
                }, "112")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var target = ParseTarget(input.First());
            var visualizer = new Visualizer(target);

            (var x, var y) = CalculateShortestHighestShot(target);

            var probe = new Probe((0, 0));
            var points = probe.SimulateTilTarget(target, (x, y));

            //visualizer.Print(points);

            return points.Max(p => p.Y).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var target = ParseTarget(input.First());
            var probe = new Probe((0, 0));

            var calculator = new TrajectoryCalculator(probe, target);

            (var x, var y) = CalculateShortestHighestShot(target);
            //var trajectories = calculator.CalculateNumberOfTrajectories((x, target.MinY), (target.MaxX, y));
            var trajectories = calculator.SimulateAllTrajectory((x, target.MinY), (target.MaxX, y));

            return trajectories.ToString();
        }

        private static (int, int) CalculateShortestHighestShot(Target target)
        {
            var squareRoots = MathExtensions.SolveQuadratic(1, 1, -2 * target.MinX);
            var positiveSquareRoot = squareRoots.X1 > squareRoots.X2 ? squareRoots.X1 : squareRoots.X2;
            var x = (int)Math.Ceiling(positiveSquareRoot);
            if (x > target.MaxX)
            {
                throw new InvalidOperationException("Oh-oh");
            }

            // Shooting upwards will make the probe land on Y=0 after the fall
            // In order to shoot the highest, the last step has to be as big as possible
            // The biggest it can be while hittin the target is the difference between the starting point and the bottom of the target
            // That's S - MinY = 0 - MinY
            var lastVerticalStep = 0 - target.MinY;

            // The upward velocity has to be [y] = [last vertical step] - 1, because we need [y] steps up, then [y] down to S and then +1 step to the target
            var y = lastVerticalStep - 1;

            //Console.WriteLine((x, y));

            return (x, y);
        }

        private static Target ParseTarget(string input)
        {
            var parts = input.Split(" ");
            var xParts = parts[2].Split("=")[1].Trim(',').Split("..").Select(int.Parse).ToArray();
            var yParts = parts[3].Split("=")[1].Split("..").Select(int.Parse).ToArray();

            return new Target(xParts[0], xParts[1], yParts[0], yParts[1]);
        }
    }
}
