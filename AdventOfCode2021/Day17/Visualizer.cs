﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day17
{
    class Visualizer
    {
        private readonly Target target;

        public Visualizer(Target target)
        {
            this.target = target;
        }

        public void Print(IEnumerable<(int X, int Y)> points)
        {
            (var minX, var maxX, var minY, var maxY) = CalculateBound(points);

            for (int y = maxY; y >= minY; y--)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    if (x == 0 && y == 0)
                    {
                        Console.Write("S");
                    }
                    else if (points.Contains((x, y)))
                    {
                        Console.Write("#");
                    }
                    else if (target.MinY <= y && y <= target.MaxY && target.MinX <= x && x <= target.MaxX)
                    {
                        Console.Write("T");
                    }
                    else
                    {
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
        }

        private (int, int, int, int) CalculateBound(IEnumerable<(int X, int Y)> points)
        {
            var minX = Math.Min(target.MinX, Math.Min(0, points.Min(p => p.Item1)));
            var maxX = Math.Max(target.MaxX, Math.Max(0, points.Max(p => p.Item1)));
            var minY = Math.Min(target.MinY, Math.Min(0, points.Min(p => p.Item2)));
            var maxY = Math.Max(target.MaxY, Math.Max(0, points.Max(p => p.Item2)));

            return (minX, maxX, minY, maxY);
        }
    }
}
