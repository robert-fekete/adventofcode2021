﻿namespace AdventOfCode2021.Day17
{
    struct Target
    {
        public readonly int MinX;
        public readonly int MaxX;
        public readonly int MinY;
        public readonly int MaxY;

        public Target(int minX, int maxX, int minY, int maxY) : this()
        {
            MinX = minX;
            MaxX = maxX;
            MinY = minY;
            MaxY = maxY;
        }
    }
}
