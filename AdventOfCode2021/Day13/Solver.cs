﻿using AdventOfCode.Framework;
using AdventOfCode.Tools.OCR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day13
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "720";

        public string SecondExpected => "AHPRPAUZ";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "6,10",
                    "0,14",
                    "9,10",
                    "0,3",
                    "10,4",
                    "4,11",
                    "6,0",
                    "6,12",
                    "4,1",
                    "0,13",
                    "10,12",
                    "3,4",
                    "3,0",
                    "8,4",
                    "1,10",
                    "2,14",
                    "8,10",
                    "9,0",
                    "",
                    "fold along y=7",
                    "fold along x=5",
                }, "17")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            (var paper, var instructions) = ParseInput(input);

            var newPaper = instructions.First().Fold(paper);
            //newPaper.Print();

            return newPaper.DotCount.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            (var paper, var instructions) = ParseInput(input);

            foreach(var instruction in instructions)
            {
                paper = instruction.Fold(paper);
            }

            var matrix = paper.GenerateMatrix();
            var ocr = new OcrAnalyzer();
            var code = ocr.ReadText(matrix);

            return code;
        }

        private static (Paper, IReadOnlyCollection<IFoldInstruction>) ParseInput(IEnumerable<string> input)
        {
            var lines = input.ToArray();
            var index = 0;

            var dots = new List<(int, int)>();
            while(!string.IsNullOrEmpty(lines[index]))
            {
                var parts = lines[index].Split(",");
                dots.Add((int.Parse(parts[0]), int.Parse(parts[1])));

                index++;
            }

            index++;

            var instructions = new List<IFoldInstruction>(); 
            for(; index < lines.Length; index++)
            {
                var axis = int.Parse(lines[index].Split("=")[1]);
                if (lines[index].Contains("x"))
                {
                    instructions.Add(new VerticalFold(axis));
                }
                else if (lines[index].Contains("y"))
                {
                    instructions.Add(new HorizontalFold(axis));
                }
                else
                {
                    throw new InvalidOperationException("Invalid fold instruction");
                }
            }

            return (new Paper(dots), instructions);
        }
    }
}
