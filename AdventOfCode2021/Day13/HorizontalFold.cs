﻿namespace AdventOfCode2021.Day13
{
    class HorizontalFold : IFoldInstruction
    {
        private readonly int axis;

        public HorizontalFold(int axis)
        {
            this.axis = axis;
        }

        public Paper Fold(Paper paper)
        {
            return paper.FoldHorizontal(axis);
        }
    }
}
