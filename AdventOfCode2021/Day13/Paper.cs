﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day13
{
    class Paper
    {
        private IEnumerable<(int, int)> dots;

        public Paper(IEnumerable<(int, int)> dots)
        {
            this.dots = dots;
        }

        public int DotCount => dots.Count();

        public Paper FoldVertical(int axis)
        {
            return Fold(axis, p => p.X, (c, a) => (CalculateFoldedCoordinate(c.X, a), c.Y));
        }

        public Paper FoldHorizontal(int axis)
        {
            return Fold(axis, p => p.Y, (c, a) => (c.X, CalculateFoldedCoordinate(c.Y, a)));
        }

        public IEnumerable<IEnumerable<bool>> GenerateMatrix()
        {
            var minX = dots.Min(p => p.Item1);
            var maxX = dots.Max(p => p.Item1);
            var minY = dots.Min(p => p.Item2);
            var maxY = dots.Max(p => p.Item2);

            var lines = new List<IEnumerable<bool>>();
            for (int y = minY; y <= maxY; y++)
            {
                var pixels = new List<bool>();
                for (int x = minX; x <= maxX; x++)
                {
                    pixels.Add(dots.Contains((x, y)));
                }
                lines.Add(pixels);
            }

            return lines;
        }

        private Paper Fold(int axis, Func<(int X, int Y), int> coordinateSelector, Func<(int X, int Y), int, (int, int)> recalculateCoordinates)
        {
            var newDots = new HashSet<(int, int)>();

            foreach (var coordinates in dots)
            {
                var newCoordinates = coordinates;
                if (coordinateSelector(newCoordinates) > axis)
                {
                    newCoordinates = recalculateCoordinates(coordinates, axis);
                }
                newDots.Add(newCoordinates);
            }

            return new Paper(newDots);
        }

        private static int CalculateFoldedCoordinate(int coordinate, int axis) => axis - (coordinate - axis);

        public void Print()
        {
            var lines = GenerateMatrix();

            foreach(var line in lines)
            {
                foreach (var pixel in line)
                {
                    if (pixel)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
