﻿namespace AdventOfCode2021.Day13
{
    interface IFoldInstruction
    {
        Paper Fold(Paper paper);
    }
}
