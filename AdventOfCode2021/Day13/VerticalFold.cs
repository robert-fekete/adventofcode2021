﻿namespace AdventOfCode2021.Day13
{
    class VerticalFold : IFoldInstruction
    {
        private readonly int axis;

        public VerticalFold(int axis)
        {
            this.axis = axis;
        }

        public Paper Fold(Paper paper)
        {
            return paper.FoldVertical(axis);
        }
    }
}
