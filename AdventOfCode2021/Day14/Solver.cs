﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day14
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "2988";

        public string SecondExpected => "3572761917024";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "NNCB",
                    "",
                    "CH -> B",
                    "HH -> N",
                    "CB -> H",
                    "NH -> C",
                    "HB -> C",
                    "HC -> B",
                    "HN -> C",
                    "NN -> C",
                    "BH -> H",
                    "NC -> B",
                    "NB -> B",
                    "BN -> B",
                    "BB -> N",
                    "BC -> B",
                    "CC -> N",
                    "CN -> C",
                }, "1588")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "NNCB",
                    "",
                    "CH -> B",
                    "HH -> N",
                    "CB -> H",
                    "NH -> C",
                    "HB -> C",
                    "HC -> B",
                    "HN -> C",
                    "NN -> C",
                    "BH -> H",
                    "NC -> B",
                    "NB -> B",
                    "BN -> B",
                    "BB -> N",
                    "BC -> B",
                    "CC -> N",
                    "CN -> C",
                }, "2188189693529")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return SolverFor(input, 10);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return SolverFor(input, 40);
        }

        private string SolverFor(IEnumerable<string> input, int cycles)
        {
            var polymer = ParsePolymer(input.First());
            var recipes = ParseRecipes(input);

            for (int i = 0; i < cycles; i++)
            {
                polymer = polymer.Grow(recipes);
            }

            var elements = polymer.GetGroups();
            var result = elements.Values.Max() - elements.Values.Min();

            return result.ToString();
        }

        private Polymer ParsePolymer(string polymer)
        {
            var pairs = new Dictionary<(char, char), long>();
            var previous = polymer.First();
            foreach (var current in polymer.Skip(1))
            {
                var key = (previous, current);
                if (!pairs.ContainsKey(key))
                {
                    pairs[key] = 0;
                }

                pairs[key]++;
                previous = current;
            }

            return new Polymer(polymer.First(), polymer.Last(), pairs);
        }

        private static Recipes ParseRecipes(IEnumerable<string> input)
        {
            var rules = new Dictionary<(char, char), char>();
            foreach(var rule in input.Where(l => l.Contains("->")))
            {
                var parts = rule.Split(" -> ");
                var key = (parts[0][0], parts[0][1]);
                rules[key] = parts[1][0];
            }

            return new Recipes(rules);
        }
    }
}
