﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day14
{
    class Polymer
    {
        private readonly char firstElement;
        private readonly char lastElement;
        private readonly Dictionary<(char, char), long> polymer;

        public Polymer(char firstElement, char lastElement, Dictionary<(char, char), long> polymer)
        {
            this.firstElement = firstElement;
            this.lastElement = lastElement;
            this.polymer = polymer;
        }

        public Polymer Grow(Recipes recipes)
        {
            var nextPolymer = new Dictionary<(char, char), long>();

            foreach(var kvp in polymer)
            {
                (var a, var b) = kvp.Key;

                var insertion = recipes.GetInsertion(a, b);
                Increment(nextPolymer, (a, insertion), kvp.Value);
                Increment(nextPolymer, (insertion, b), kvp.Value);
            }

            return new Polymer(firstElement, lastElement, nextPolymer);
        }

        public Dictionary<char, long> GetGroups()
        {
            var groups = new Dictionary<char, long>();
            foreach(var kvp in polymer)
            {
                (var a, var b) = kvp.Key;
                Increment(groups, a, kvp.Value);
                Increment(groups, b, kvp.Value);
            }

            groups[firstElement]++;
            groups[lastElement]++;

            foreach(var key in groups.Keys)
            {
                groups[key] /= 2;
            }

            return groups;
        }

        private static void Increment<T>(Dictionary<T, long> polymer, T key, long value) where T : notnull
        {
            if (!polymer.ContainsKey(key))
            {
                polymer[key] = 0;
            }

            polymer[key] += value;
        }

        public override string ToString()
        {
            return string.Join("", polymer);
        }
    }
}
