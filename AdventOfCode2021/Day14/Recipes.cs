﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2021.Day14
{
    internal class Recipes
    {
        private Dictionary<(char, char), char> rules;

        public Recipes(Dictionary<(char, char), char> rules)
        {
            this.rules = rules;
        }

        public char GetInsertion(char previousElement, char nextElement)
        {
            return rules[(previousElement, nextElement)];
        }
    }
}