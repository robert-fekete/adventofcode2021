﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day1
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "1564";

        public string SecondExpected => "1611";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "199",
                    "200",
                    "208",
                    "210",
                    "200",
                    "207",
                    "240",
                    "269",
                    "260",
                    "263",
                }, "7")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "199",
                    "200",
                    "208",
                    "210",
                    "200",
                    "207",
                    "240",
                    "269",
                    "260",
                    "263",
                }, "5")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var numbers = input.Select(int.Parse);

            int prev = int.MaxValue;
            int counter = 0;
            foreach(var number in numbers)
            {
                if (prev < number)
                {
                    counter++;
                }
                prev = number;
            }

            return counter.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var numbers = input.Select(int.Parse).ToArray();

            var iter = numbers.Take(3).Sum();
            var counter = 0;
            for (int i = 3; i < numbers.Length; i++)
            {
                var next = iter + numbers[i] - numbers[i - 3];
                if (iter < next)
                {
                    counter++;
                }
                iter = next;
            }

            return counter.ToString();
        }
    }
}
