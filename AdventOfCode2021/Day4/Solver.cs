﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day4
{
    [Solver]
    public class Solver : ISolver
    {
        public Solver(int day) => DayNumber = day;

        public int DayNumber { get; }

        public string FirstExpected => "49686";

        public string SecondExpected => "26878";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1",
                    "",
                    "22 13 17 11  0",
                    " 8  2 23  4 24",
                    "21  9 14 16  7",
                    " 6 10  3 18  5",
                    " 1 12 20 15 19",
                    "",
                    " 3 15  0  2 22",
                    " 9 18 13 17  5",
                    "19  8  7 25 23",
                    "20 11 10 24  4",
                    "14 21 16 12  6",
                    "",
                    "14 21 17 24  4",
                    "10 16 15  9 19",
                    "18  8 23 26 20",
                    "22 11 13  6  5",
                    " 2  0 12  3  7",
                }, "4512")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1",
                    "",
                    "22 13 17 11  0",
                    " 8  2 23  4 24",
                    "21  9 14 16  7",
                    " 6 10  3 18  5",
                    " 1 12 20 15 19",
                    "",
                    " 3 15  0  2 22",
                    " 9 18 13 17  5",
                    "19  8  7 25 23",
                    "20 11 10 24  4",
                    "14 21 16 12  6",
                    "",
                    "14 21 17 24  4",
                    "10 16 15  9 19",
                    "18  8 23 26 20",
                    "22 11 13  6  5",
                    " 2  0 12  3  7",
                }, "1924")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var draws = input.First().Split(",").Select(int.Parse);

            var bingoTables = ParseBingoTables(input);

            var score = PlayBingoToWin(bingoTables, draws);

            return score.ToString();
        }

        private static int PlayBingoToWin(IReadOnlyCollection<BingoTable> bingoTables, IEnumerable<int> draws)
        {
            foreach (var draw in draws)
            {
                foreach (var bingoTable in bingoTables)
                {
                    bingoTable.Feed(draw);
                }

                var winner = bingoTables.FirstOrDefault(bt => bt.IsBingo());
                if (winner is not null)
                {
                    var unmarkedNumbers = winner.GetUnmarkedNumbers();
                    return unmarkedNumbers.Sum() * draw;
                }
            }

            throw new InvalidOperationException("Nobody won");
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var draws = input.First().Split(",").Select(int.Parse);

            var bingoTables = ParseBingoTables(input);

            var score = PlayBingoToLose(bingoTables, draws);

            return score.ToString();
        }

        private static int PlayBingoToLose(IReadOnlyCollection<BingoTable> bingoTables, IEnumerable<int> draws)
        {
            var remainingTables = bingoTables;

            foreach (var draw in draws)
            {
                foreach (var bingoTable in remainingTables)
                {
                    bingoTable.Feed(draw);
                }

                if (remainingTables.Count == 1 && remainingTables.All(bt => bt.IsBingo()))
                {
                    var loser = remainingTables.Single();
                    var unmarkedNumbers = loser.GetUnmarkedNumbers();
                    return unmarkedNumbers.Sum() * draw;
                }

                remainingTables = remainingTables.Where(bt => !bt.IsBingo()).ToArray();
            }

            throw new InvalidOperationException("Nobody won");
        }

        private IReadOnlyCollection<BingoTable> ParseBingoTables(IEnumerable<string> input)
        {
            int i = 2;
            var bingoTables = new List<BingoTable>();
            while (i < input.Count())
            {
                var bingoTable = ParseBingoTable(input.Skip(i).Take(5));
                bingoTables.Add(bingoTable);

                i += 6;
            }

            return bingoTables;
        }

        private BingoTable ParseBingoTable(IEnumerable<string> rowsRaw)
        {
            var rows = rowsRaw.Select(r => r.Split(" ").Where(s => s != string.Empty).Select(int.Parse).ToArray());
            var columns = new List<int[]>();
            for (int i = 0; i < rows.First().Length; i++)
            {
                columns.Add(rows.Select(r => r[i]).ToArray());
            }

            return new BingoTable(rows, columns);
        }
    }
}
