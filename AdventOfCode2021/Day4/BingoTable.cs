﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day4
{
    internal class BingoTable
    {
        private readonly IEnumerable<int[]> rows;
        private readonly IEnumerable<int[]> columns;

        private readonly HashSet<int> markedNumbers = new HashSet<int>();

        public BingoTable(IEnumerable<int[]> rows, IEnumerable<int[]> columns)
        {
            this.rows = rows;
            this.columns = columns;
        }

        internal void Feed(int draw)
        {
            markedNumbers.Add(draw);
        }

        internal bool IsBingo()
        {
            return IsFullSetMarked(rows) || IsFullSetMarked(columns);
        }

        private bool IsFullSetMarked(IEnumerable<int[]> set)
        {
            return set.Any(r => r.All(n => markedNumbers.Contains(n)));
        }

        internal IReadOnlyCollection<int> GetUnmarkedNumbers()
        {
            return rows.SelectMany(r => r.Where(n => !markedNumbers.Contains(n))).ToArray();
        }
    }
}